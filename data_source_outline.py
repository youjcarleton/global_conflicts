'''
Josh You and Lauren Yoo
Data Source Class
April 22, 2015

Our web application provides information on a global conflict that the user is interested in. 
Our data is from Uppsala Conflict Data Program (DCDP)
'''
class DataSource:
    def __init__(self):
        """Constructor for the DataSource database interface class.
        """
        
 
    def getConflictList(self):
        """Returns a list of the id numbers of all conflicts in the UCDP data base."""
        # Implementation will eventually go here. In the
        # meantime, just return an object of the right type.
        return []
     
    def getAllCountriesInRegion(self, region):
        """Returns a list of all countries in a given region"""
        return []
        
    def getConflictByLocation(self, location):
        """"Returns a list of conflict ids for conflicts that happened in the given location (country)"""
        return []
        
    def getConflictByYear(self, year):
        """Returns a list of conflicts that happened in the given year"""
        return []
        
    def getConflictByIntensity(self):
        """Returns a sorted list of conflicts by the degree by intensity"""
        return []
        
    def getConflictByParticipant(self, participant[]):
         """Given a list of parties, 
        returns a list of conflict ids involving the given partipants"""
        return []
        
    def getName(self, conflict_id):
        """Returns the name of the given conflict, if available"""
        return ""
             
    def getLocation(self, conflict_id):
        '''Returns the location where the conflict occurred'''
        return ""
        
    def getStartDate(self, conflict_id):
        '''Returns start date of the given conflict'''
        return ""
        
    def getEndDate(self, conflict_id):
        '''Returns end date of the given conflict'''
        return ""
        
    def getSideA(self, conflict_id):
        '''Returns a list of participants designated Side A in a given conflict'''
        return []
        
    def getSideB(self, conflict_id):
        '''Returns a list of participants designated Side A in a given conflict'''
        return []
        
    def getIntensity(self, conflict_id):
        '''Returns intensity of the given conflict_id'''
        return 0
        
    def getSoldierCasualties(self, conflict_id):
        '''Returns number of soldier deaths of the given conflict_id''' 
        return 0
        
    def getCivilianCasualties(self, conflict_id):
        '''Returns number of civilian deaths of the given conflict_id''' 
        return 0
        
    def getConflictType(self, conflict_id):
        '''Returns the type of the given conflict_id 
        (Extra-systemic, interstate, internal, or international''' 
        return ""