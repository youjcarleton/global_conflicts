import unittest
from data_source import DataSource

class DataSourceTest(unittest.TestCase):

    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        print "DataSourceTest:setUp_:begin"
        ## do something...
        print "DataSourceTest:setUp_:end"
     
    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        print "DataSourceTest:tearDown_:begin"
        ## do something...
        print "DataSourceTest:tearDown_:end"

    # tests that SortByStartYear correctly sorts a dictionary of conflicts
    # and start years
    def testSortByStartYear(self):
        conflicts = {1:1999, 2:1962, 3:1995, 4:2000, 5:1983, 6:1981}
        assert DataSource.sortByStartYear(conflicts) == [4,1,3,5,6,2]

if __name__ == "__main__":
   unittest.main()