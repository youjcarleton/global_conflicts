Name: 
Global Conflicts



Members:
Lauren Yoo and Josh You



URL:
http://thacker.mathcs.carleton.edu/cs257/youj/index.py



Features:

-For each region, shows a list of the countries in that region that are in the database
-For each country, shows a table of conflicts involving that country, with information on dates, participants,
intensity, and type of conflict.

-Users search by year and find a table of conflicts that occurred in that year
-Displays a list of every country in the database with the number of conflicts that occurred there.