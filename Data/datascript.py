import sys
import csv

reader = csv.reader(sys.stdin)
output = open("out.txt", "w")

conflicts = {}
firstRow = reader.next()

index_list = [1, 2, 15, 18]

counter = 0
conflict_id = 0

for row in reader:
    conflict_data = []
    query = "INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, endDate, startYear, endYear, region)"
    if row[17] == "1":
        conflict_data.append(conflict_id)
        #query += str(conflict_id)
        sideAString = row[2].replace(',', '')
        sideAString = sideAString.replace('\'', '')
        sideBString = row[4].replace(',', '')
        sideBString = sideBString.replace('\'', '')
        query += " VALUES (%s, '%s', '%s', '%s', %s, %s, %s, '%s', '%s', %s, %s, %s);" % (str(conflict_id), row[1], sideAString, sideBString, row[7], row[10], row[12], row[15], row[18], row[15][-4:], row[18][-4:], row[25])
        query += '\n'
        output.write(query)
        conflict_id += 1
    counter += 1
