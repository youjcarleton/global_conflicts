
import sys
import csv

# Set up the CSV reader object to read from standard input and skip the title row.
reader = csv.reader(sys.stdin)
headingRow = reader.next()[1:]

cities = {}
for row in reader:
    row = map(str.strip, row)
    assert len(row) == len(headingRow) + 1
    cities[row[0]] = row[1:]

for city in cities:
	print city
    for k in range(len(headingRow)):
        cityRow = cities[city]
        yearString = headingRow[k]
        populationString = cityRow[k].replace(',', '') # if the CSV numbers have commas in them, delete them
        if populationString == '':
            populationString = '0'
        query = "INSERT INTO populations (city, year, population)"
        query +=   " VALUES ('%s', %s, %s);" % (city, yearString, populationString)
        print query


ConflictId	Location	SideA	SideA2nd	SideB	SideBID	SideB2nd	Incompatibility	TerritoryName	Year	IntensityLevel	CumulativeIntensity	TypeOfConflict	StartDate	StartPrec	StartDate2	StartPrec2	EpEnd	EpEndDate	EpEndPrec	GWNoA	GWNoA2nd	GWNoB	GWNoB2nd	GWNoLoc	Region	Version