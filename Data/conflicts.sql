DROP TABLE IF EXISTS countries;
CREATE TABLE countries (
    name text,
    region int
);

INSERT INTO countries (name, region) VALUES ('Bolivia', 5);
INSERT INTO countries (name, region) VALUES ('Cambodia (Kampuchea)', 3);
INSERT INTO countries (name, region) VALUES ('China', 3);
INSERT INTO countries (name, region) VALUES ('Greece', 1);
INSERT INTO countries (name, region) VALUES ('Indonesia', 3);
INSERT INTO countries (name, region) VALUES ('Iran', 2);
INSERT INTO countries (name, region) VALUES ('Israel', 2);
INSERT INTO countries (name, region) VALUES ('Laos', 3);
INSERT INTO countries (name, region) VALUES ('Philippines', 3);
INSERT INTO countries (name, region) VALUES ('Russia (Soviet Union)', 1);
INSERT INTO countries (name, region) VALUES ('France', 1);
INSERT INTO countries (name, region) VALUES ('Thailand', 3);
INSERT INTO countries (name, region) VALUES ('Albania', 1);
INSERT INTO countries (name, region) VALUES ('United Kingdom', 1);
INSERT INTO countries (name, region) VALUES ('Vietnam (North Vietnam)', 3);
INSERT INTO countries (name, region) VALUES ('Hyderabadh', 3);
INSERT INTO countries (name, region) VALUES ('India', 3);
INSERT INTO countries (name, region) VALUES ('Pakistan', 3);
INSERT INTO countries (name, region) VALUES ('Madagascar (Malagasy)', 4);
INSERT INTO countries (name, region) VALUES ('Paraguay', 5);
INSERT INTO countries (name, region) VALUES ('Myanmar (Burma)', 3);
INSERT INTO countries (name, region) VALUES ('Costa Rica', 5);
INSERT INTO countries (name, region) VALUES ('Egypt', 2);
INSERT INTO countries (name, region) VALUES ('Iraq', 2);
INSERT INTO countries (name, region) VALUES ('Jordan', 2);
INSERT INTO countries (name, region) VALUES ('Lebanon', 2);
INSERT INTO countries (name, region) VALUES ('Syria', 2);
INSERT INTO countries (name, region) VALUES ('Malaysia', 3);
INSERT INTO countries (name, region) VALUES ('Yemen (North Yemen)', 2);
INSERT INTO countries (name, region) VALUES ('Taiwan', 3);
INSERT INTO countries (name, region) VALUES ('Guatemala', 5);
INSERT INTO countries (name, region) VALUES ('North Korea', 3);
INSERT INTO countries (name, region) VALUES ('South Korea', 3);
INSERT INTO countries (name, region) VALUES ('United States of America', 5);
INSERT INTO countries (name, region) VALUES ('Kenya', 4);
INSERT INTO countries (name, region) VALUES ('Cuba', 5);
INSERT INTO countries (name, region) VALUES ('Morocco', 4);
INSERT INTO countries (name, region) VALUES ('Tunisia', 4);
INSERT INTO countries (name, region) VALUES ('Algeria', 4);
INSERT INTO countries (name, region) VALUES ('Argentina', 5);
INSERT INTO countries (name, region) VALUES ('Cyprus', 1);
INSERT INTO countries (name, region) VALUES ('South Vietnam', 3);
INSERT INTO countries (name, region) VALUES ('Hungary', 1);
INSERT INTO countries (name, region) VALUES ('Cameroon', 4);
INSERT INTO countries (name, region) VALUES ('Honduras', 5);
INSERT INTO countries (name, region) VALUES ('Nicaragua', 5);
INSERT INTO countries (name, region) VALUES ('Mauritania', 4);
INSERT INTO countries (name, region) VALUES ('Oman', 2);
INSERT INTO countries (name, region) VALUES ('Angola', 4);
INSERT INTO countries (name, region) VALUES ('DR Congo (Zaire)', 4);
INSERT INTO countries (name, region) VALUES ('Ethiopia', 4);
INSERT INTO countries (name, region) VALUES ('Somalia', 4);
INSERT INTO countries (name, region) VALUES ('Nepal', 3);
INSERT INTO countries (name, region) VALUES ('Brunei', 3);
INSERT INTO countries (name, region) VALUES ('Netherlands', 1);
INSERT INTO countries (name, region) VALUES ('Venezuela', 5);
INSERT INTO countries (name, region) VALUES ('Guinea-Bissau', 4);
INSERT INTO countries (name, region) VALUES ('Sudan', 4);
INSERT INTO countries (name, region) VALUES ('Gabon', 4);
INSERT INTO countries (name, region) VALUES ('Mozambique', 4);
INSERT INTO countries (name, region) VALUES ('South Yemen', 2);
INSERT INTO countries (name, region) VALUES ('Burundi', 4);
INSERT INTO countries (name, region) VALUES ('Chad', 4);
INSERT INTO countries (name, region) VALUES ('Colombia', 5);
INSERT INTO countries (name, region) VALUES ('Dominican Republic', 5);
INSERT INTO countries (name, region) VALUES ('Peru', 5);
INSERT INTO countries (name, region) VALUES ('Ghana', 4);
INSERT INTO countries (name, region) VALUES ('Nigeria', 4);
INSERT INTO countries (name, region) VALUES ('South Africa', 4);
INSERT INTO countries (name, region) VALUES ('El Salvador', 5);
INSERT INTO countries (name, region) VALUES ('Guinea', 4);
INSERT INTO countries (name, region) VALUES ('Sri Lanka', 3);
INSERT INTO countries (name, region) VALUES ('Uganda', 4);
INSERT INTO countries (name, region) VALUES ('Zimbabwe (Rhodesia)', 4);
INSERT INTO countries (name, region) VALUES ('Uruguay', 5);
INSERT INTO countries (name, region) VALUES ('Chile', 5);
INSERT INTO countries (name, region) VALUES ('Bangladesh', 3);
INSERT INTO countries (name, region) VALUES ('Turkey', 2);
INSERT INTO countries (name, region) VALUES ('Eritrea', 4);
INSERT INTO countries (name, region) VALUES ('Afghanistan', 3);
INSERT INTO countries (name, region) VALUES ('Saudi Arabia', 2);
INSERT INTO countries (name, region) VALUES ('Liberia', 4);
INSERT INTO countries (name, region) VALUES ('Spain', 1);
INSERT INTO countries (name, region) VALUES ('Gambia', 4);
INSERT INTO countries (name, region) VALUES ('Grenada', 5);
INSERT INTO countries (name, region) VALUES ('Burkina Faso', 4);
INSERT INTO countries (name, region) VALUES ('Mali', 4);
INSERT INTO countries (name, region) VALUES ('Suriname', 5);
INSERT INTO countries (name, region) VALUES ('Togo', 4);
INSERT INTO countries (name, region) VALUES ('Libya', 4);
INSERT INTO countries (name, region) VALUES ('Comoros', 4);
INSERT INTO countries (name, region) VALUES ('Panama', 5);
INSERT INTO countries (name, region) VALUES ('Papua New Guinea', 3);
INSERT INTO countries (name, region) VALUES ('Rumania', 1);
INSERT INTO countries (name, region) VALUES ('Kuwait', 2);
INSERT INTO countries (name, region) VALUES ('Niger', 4);
INSERT INTO countries (name, region) VALUES ('Rwanda', 4);
INSERT INTO countries (name, region) VALUES ('Senegal', 4);
INSERT INTO countries (name, region) VALUES ('Trinidad and Tobago', 5);
INSERT INTO countries (name, region) VALUES ('Djibouti', 4);
INSERT INTO countries (name, region) VALUES ('Georgia', 1);
INSERT INTO countries (name, region) VALUES ('Haiti', 5);
INSERT INTO countries (name, region) VALUES ('Sierra Leone', 4);
INSERT INTO countries (name, region) VALUES ('Serbia (Yugoslavia)', 1);
INSERT INTO countries (name, region) VALUES ('Azerbaijan', 1);
INSERT INTO countries (name, region) VALUES ('Bosnia-Herzegovina', 1);
INSERT INTO countries (name, region) VALUES ('Croatia', 1);
INSERT INTO countries (name, region) VALUES ('Moldova', 1);
INSERT INTO countries (name, region) VALUES ('Tajikistan', 3);
INSERT INTO countries (name, region) VALUES ('Mexico', 5);
INSERT INTO countries (name, region) VALUES ('Ecuador', 5);
INSERT INTO countries (name, region) VALUES ('Congo', 4);
INSERT INTO countries (name, region) VALUES ('Lesotho', 4);
INSERT INTO countries (name, region) VALUES ('Uzbekistan', 3);
INSERT INTO countries (name, region) VALUES ('Central African Republic', 4);
INSERT INTO countries (name, region) VALUES ('Macedonia', 1);
INSERT INTO countries (name, region) VALUES ('Former Yugoslav Republic', 1);
INSERT INTO countries (name, region) VALUES ('Ivory Coast', 4);
INSERT INTO countries (name, region) VALUES ('Australia', 3);
INSERT INTO countries (name, region) VALUES ('Tanzania', 4);
INSERT INTO countries (name, region) VALUES ('South Sudan', 4);


DROP TABLE IF EXISTS conflicts;
CREATE TABLE conflicts (
    conflictId int, 
    location text, 
    sideA text, 
    sideB text, 
    incompatibility int, 
    intensityLevel int, 
    typeOfConflict int, 
    startDate text, 
    endDate text, 
    startYear int,
    endYear int,
    region int
);

INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (0, 'Bolivia', 'Government of Bolivia', 'Popular Revolutionary Movement', 2, 

2, 3, '6/30/1946', '7/21/1946', 1946, 1946, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (1, 'Bolivia', 'Government of Bolivia', 'MNR', 2, 1, 3, '4/9/1952', 

'4/12/1952', 1952, 1952, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (2, 'Bolivia', 'Government of Bolivia', 'ELN', 2, 1, 3, '3/31/1967', 

'10/16/1967', 1967, 1967, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (3, 'Cambodia (Kampuchea)', 'Government of France', 'Khmer Issarak', 1, 1, 1, 

'8/31/1946', '11/9/1953', 1946, 1953, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (4, 'China', 'Government of China', 'PLA', 2, 2, 3, '12/31/1946', '12/8/1949', 

1946, 1949, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (5, 'Greece', 'Government of Greece', 'DSE', 2, 2, 3, '3/31/1946', 

'10/16/1949', 1946, 1949, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (6, 'Indonesia', 'Government of Netherlands', 'Indonesian Peoples Army', 1, 1, 

1, '12/31/1946', '8/15/1949', 1946, 1949, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (7, 'Iran', 'Government of Iran', 'KDPI', 1, 1, 4, '5/31/1946', '12/16/1946', 

1946, 1946, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (8, 'Iran', 'Government of Iran', 'KDPI', 1, 1, 3, '12/31/1966', '12/31/1968', 

1966, 1968, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (9, 'Iran', 'Government of Iran', 'KDPI', 1, 1, 3, '12/31/1979', '12/31/1988', 

1979, 1988, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (10, 'Iran', 'Government of Iran', 'KDPI', 1, 1, 3, '7/6/1990', '12/31/1990', 

1990, 1990, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (11, 'Iran', 'Government of Iran', 'KDPI', 1, 1, 3, '9/12/1993', '12/31/1993', 

1993, 1993, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (12, 'Iran', 'Government of Iran', 'KDPI', 1, 1, 3, '7/28/1996', '12/31/1996', 

1996, 1996, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (13, 'Iran', 'Government of Iran', 'Republic of Azerbaijan', 1, 1, 4, 

'12/16/1946', '12/16/1946', 1946, 1946, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (14, 'Israel', 'Government of United Kingdom', 'IZL [Etzel]', 1, 1, 1, 

'12/31/1946', '12/31/1946', 1946, 1946, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (15, 'Laos', 'Government of France', 'Lao Issara', 1, 1, 1, '3/31/1946', 

'12/31/1953', 1946, 1953, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (16, 'Philippines', 'Government of Philippines', 'Huk', 2, 2, 3, '7/31/1946', 

'12/31/1954', 1946, 1954, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (17, 'Philippines', 'Government of Philippines', 'CPP', 2, 1, 3, '9/30/1969', 

'12/13/1995', 1969, 1995, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (18, 'Philippines', 'Government of Philippines', 'CPP', 2, 1, 3, '11/23/1997', 

'12/22/1997', 1997, 1997, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (19, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'Forest 

Brothers', 1, 1, 3, '12/31/1946', '12/31/1948', 1946, 1948, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (20, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'LNPA 

LTS(p)A', 1, 1, 3, '12/31/1946', '12/31/1946', 1946, 1946, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (21, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'BDPS', 

1, 1, 3, '6/30/1946', '12/31/1948', 1946, 1948, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (22, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'UPA', 1, 

1, 3, '12/31/1946', '12/31/1950', 1946, 1950, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (23, 'France, Thailand', 'Government of France', 'Government of Thailand', 1, 

1, 2, '5/7/1946', '11/17/1946', 1946, 1946, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (24, 'Albania, United Kingdom', 'Government of Albania', 'Government of United 

Kingdom', 1, 1, 2, '10/22/1946', '12/31/1946', 1946, 1946, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (25, 'Vietnam (North Vietnam)', 'Government of France', 'Viet minh', 1, 2, 1, 

'11/20/1946', '7/20/1954', 1946, 1954, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (26, 'China', 'Government of China', 'Taiwanese insurgents', 1, 2, 3, 

'2/28/1947', '3/24/1947', 1947, 1947, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (27, 'Hyderabadh', 'Government of Hyderabad', 'CPI', 2, 2, 3, '6/30/1947', 

'9/18/1948', 1947, 1948, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (28, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 2, 

2, '12/31/1948', '12/31/1948', 1948, 1948, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (29, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 2, 

2, '12/31/1964', '12/15/1965', 1964, 1965, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (30, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 2, 

2, '12/3/1971', '12/17/1971', 1971, 1971, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (31, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 1, 

2, '12/31/1984', '12/31/1984', 1984, 1984, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (32, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 1, 

2, '12/31/1987', '12/31/1987', 1987, 1987, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (33, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 1, 

2, '12/31/1989', '12/31/1992', 1989, 1992, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (34, 'India, Pakistan', 'Government of India', 'Government of Pakistan', 1, 1, 

2, '12/31/1996', '11/26/2003', 1996, 2003, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (35, 'Madagascar (Malagasy)', 'Government of France', 'MDRM', 1, 2, 1, 

'3/29/1947', '12/31/1947', 1947, 1947, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (36, 'Paraguay', 'Government of Paraguay', 'Opposition coalition (Febreristas 

Liberals and Communists)', 2, 2, 3, '3/31/1947', '8/21/1947', 1947, 1947, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (37, 'Paraguay', 'Government of Paraguay', 'Military faction (forces of 

Alfredo Stroessner)', 2, 1, 3, '5/5/1954', '5/5/1954', 1954, 1954, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (38, 'Paraguay', 'Government of Paraguay', 'Military faction (forces of Andres 

Rodriguez)', 2, 1, 3, '2/3/1989', '2/3/1989', 1989, 1989, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (39, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNU', 1, 2, 3, 

'1/15/1949', '11/22/1992', 1949, 1992, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (40, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNU', 1, 1, 3, 

'12/31/1994', '7/13/1995', 1994, 1995, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (41, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNU', 1, 1, 3, 

'2/23/1997', '7/14/1998', 1997, 1998, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (42, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'DKBA 5 KNU', 1, 1, 

3, '3/3/2000', '12/31/2011', 2000, 2011, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (43, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'CPB', 2, 2, 3, 

'2/29/1948', '12/31/1988', 1948, 1988, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (44, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'ABSDF', 2, 1, 3, 

'12/31/1990', '12/31/1992', 1990, 1992, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (45, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'ABSDF', 2, 1, 3, 

'12/31/1994', '12/31/1994', 1994, 1994, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (46, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'Mujahid Party', 1, 

1, 3, '1/31/1948', '11/15/1961', 1948, 1961, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (47, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'RPF', 1, 1, 3, 

'12/31/1964', '12/31/1978', 1964, 1978, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (48, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'RSO', 1, 1, 3, 

'12/29/1991', '12/29/1991', 1991, 1991, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (49, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'RSO', 1, 1, 3, 

'5/15/1994', '6/23/1994', 1994, 1994, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (50, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'NMSP', 1, 1, 3, 

'2/28/1949', '12/31/1963', 1949, 1963, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (51, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'NMSP', 1, 1, 3, 

'2/13/1990', '3/22/1990', 1990, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (52, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'BMA', 1, 1, 3, 

'12/23/1996', '12/23/1996', 1996, 1996, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (53, 'Costa Rica', 'Government of Costa Rica', 'National Liberation Army', 2, 

2, 3, '3/3/1948', '4/20/1948', 1948, 1948, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (54, 'Hyderabadh, India', 'Government of Hyderabad', 'Government of India', 1, 

2, 2, '9/13/1948', '9/18/1948', 1948, 1948, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (55, 'India', 'Government of India', 'CPI', 2, 2, 3, '9/18/1948', 

'12/31/1951', 1948, 1951, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (56, 'India', 'Government of India', 'CPI-ML', 2, 1, 3, '12/31/1969', 

'12/31/1971', 1969, 1971, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (57, 'India', 'Government of India', 'PWG', 2, 1, 3, '12/31/1990', 

'12/31/1994', 1990, 1994, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (58, 'Egypt, Iraq, Israel, Jordan, Lebanon, Syria', 'Government of Egypt 

Government of Iraq Government of Jordan Government of Lebanon Government of Syria', 'Government of Israel', 1, 1, 2, 

'4/15/1948', '1/7/1949', 1948, 1949, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (59, 'Malaysia', 'Government of United Kingdom', 'CPM', 1, 1, 1, '6/19/1948', 

'8/31/1957', 1948, 1957, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (60, 'Yemen (North Yemen)', 'Government of Yemen (North Yemen)', 'Opposition 

coalition', 2, 2, 3, '3/15/1948', '3/15/1948', 1948, 1948, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (61, 'Yemen (North Yemen)', 'Government of Yemen (North Yemen)', 'Royalists', 

2, 1, 4, '10/31/1962', '5/23/1970', 1962, 1970, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (62, 'Yemen (North Yemen)', 'Government of Yemen (North Yemen)', 'NDF', 2, 1, 

3, '3/31/1979', '5/31/1982', 1979, 1982, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (63, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'PNDF', 1, 1, 3, 

'12/31/1949', '5/5/1950', 1949, 1950, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (64, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KIO', 1, 1, 3, 

'2/28/1961', '12/31/1992', 1961, 1992, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (65, 'China, Taiwan', 'Government of China', 'Government of Taiwan', 3, 1, 2, 

'10/31/1949', '12/31/1950', 1949, 1950, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (66, 'China, Taiwan', 'Government of China', 'Government of Taiwan', 3, 2, 2, 

'12/31/1954', '12/31/1954', 1954, 1954, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (67, 'China, Taiwan', 'Government of China', 'Government of Taiwan', 3, 2, 2, 

'12/31/1958', '12/31/1958', 1958, 1958, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (68, 'Guatemala', 'Government of Guatemala', 'Military faction', 2, 1, 3, 

'7/18/1949', '7/19/1949', 1949, 1949, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (69, 'Guatemala', 'Government of Guatemala', 'Forces of Carlos Castillo 

Armas', 2, 1, 3, '6/18/1954', '6/27/1954', 1954, 1954, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (70, 'Guatemala', 'Government of Guatemala', 'FAR I', 2, 1, 3, '7/31/1963', 

'12/31/1963', 1963, 1963, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (71, 'Guatemala', 'Government of Guatemala', 'URNG', 2, 1, 3, '12/31/1965', 

'12/31/1995', 1965, 1995, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (72, 'Israel', 'Government of Israel', 'PNA', 1, 1, 3, '12/31/1949', 

'12/31/1996', 1949, 1996, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (73, 'Israel', 'Government of Israel', 'Hamas PIJ', 1, 1, 3, '11/1/2000', 

'12/31/2012', 2000, 2012, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (74, 'North Korea, South Korea', 'Government of North Korea', 'Government of 

South Korea', 1, 2, 2, '5/31/1949', '7/27/1953', 1949, 1953, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (75, 'China', 'Government of China', 'Tibet', 1, 1, 3, '10/7/1950', 

'10/9/1950', 1950, 1950, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (76, 'China', 'Government of China', 'Tibet', 1, 2, 3, '5/31/1956', 

'12/31/1956', 1956, 1956, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (77, 'China', 'Government of China', 'Tibet', 1, 2, 3, '3/10/1959', 

'4/23/1959', 1959, 1959, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (78, 'Indonesia', 'Government of Indonesia', 'Republic of South Moluccas', 1, 

2, 3, '8/5/1950', '11/15/1950', 1950, 1950, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (79, 'United States of America', 'Government of United States of America', 

'Puerto Rican Nationalist Party', 1, 1, 1, '10/30/1950', '11/1/1950', 1950, 1950, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (80, 'Egypt, United Kingdom', 'Government of Egypt', 'Government of United 

Kingdom', 1, 1, 2, '10/18/1951', '12/31/1952', 1951, 1952, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (81, 'Thailand', 'Government of Thailand', 'Military faction (navy)', 2, 1, 3, 

'6/30/1951', '7/1/1951', 1951, 1951, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (82, 'Thailand', 'Government of Thailand', 'CPT', 2, 1, 3, '10/31/1974', 

'12/31/1982', 1974, 1982, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (83, 'Kenya', 'Government of United Kingdom', 'Mau Mau', 1, 2, 1, 

'10/22/1952', '12/31/1956', 1952, 1956, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (84, 'Cuba', 'Government of Cuba', 'M-26-7', 2, 1, 3, '7/26/1953', 

'7/27/1953', 1953, 1953, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (85, 'Cuba', 'Government of Cuba', 'M-26-7', 2, 2, 3, '12/5/1956', 

'12/31/1958', 1956, 1958, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (86, 'Cuba', 'Government of Cuba', 'Cuban Revolutionary Council', 2, 1, 4, 

'4/17/1961', '4/20/1961', 1961, 1961, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (87, 'Indonesia', 'Government of Indonesia', 'Darul Islam', 2, 2, 3, 

'12/31/1953', '12/31/1953', 1953, 1953, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (88, 'Indonesia', 'Government of Indonesia', 'Darul Islam Permesta Movement 

PRRI', 2, 2, 3, '4/17/1958', '12/31/1961', 1958, 1961, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (89, 'Morocco', 'Government of France', 'Istiqlal', 1, 1, 1, '11/7/1953', 

'3/2/1956', 1953, 1956, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (90, 'Tunisia', 'Government of France', 'National Liberation Army', 1, 1, 1, 

'4/30/1953', '3/20/1956', 1953, 1956, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (91, 'Algeria', 'Government of France', 'FLN ', 1, 1, 1, '11/29/1954', 

'3/19/1962', 1954, 1962, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (92, 'Argentina', 'Government of Argentina', 'Military faction (forces of 

Eduardo A. Lonardi Doucet) Military faction (forces of Samuel Toranzo Calder¢n)', 2, 1, 3, '6/16/1955', '9/19/1955', 1955, 

1955, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (93, 'Argentina', 'Government of Argentina', 'Military faction (colorados)', 

2, 1, 3, '4/2/1963', '9/22/1963', 1963, 1963, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (94, 'Argentina', 'Government of Argentina', 'ERP Montoneros', 2, 1, 3, 

'8/11/1974', '12/31/1977', 1974, 1977, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (95, 'Cyprus', 'Government of United Kingdom', 'EOKA', 1, 1, 1, '6/2/1955', 

'2/19/1959', 1955, 1959, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (96, 'South Vietnam', 'Government of South Vietnam', 'FNL', 1, 2, 4, 

'4/30/1955', '12/31/1964', 1955, 1964, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (97, 'Hungary, Russia (Soviet Union)', 'Government of Hungary', 'Government of 

Russia (Soviet Union)', 2, 2, 2, '10/23/1956', '11/14/1956', 1956, 1956, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (98, 'India', 'Government of India', 'NNC', 1, 1, 3, '12/31/1956', 

'12/31/1959', 1956, 1959, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (99, 'India', 'Government of India', 'NNC', 1, 1, 3, '12/31/1961', 

'6/15/1968', 1961, 1968, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (100, 'India', 'Government of India', 'NSCN - IM', 1, 1, 3, '8/5/1992', 

'8/1/1997', 1992, 1997, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (101, 'India', 'Government of India', 'NSCN - IM', 1, 1, 3, '8/2/2000', 

'12/31/2000', 2000, 2000, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (102, 'Egypt, France, Israel, United Kingdom', 'Government of Egypt', 

'Government of France Government of Israel Government of United Kingdom', 1, 2, 2, '10/31/1956', '11/6/1956', 1956, 1956, 

2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (103, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNPP', 1, 1, 3, 

'12/31/1957', '12/31/1957', 1957, 1957, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (104, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNPP', 1, 1, 3, 

'12/31/1987', '12/31/1987', 1987, 1987, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (105, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNPP', 1, 1, 3, 

'9/22/1992', '11/6/1992', 1992, 1992, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (106, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNPP', 1, 1, 3, 

'3/6/1996', '10/6/1996', 1996, 1996, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (107, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'KNPP', 1, 1, 3, 

'1/16/2005', '1/25/2005', 2005, 2005, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (108, 'Cameroon', 'Government of France', 'UPC', 1, 1, 1, '9/30/1957', 

'12/31/1959', 1957, 1959, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (109, 'Honduras, Nicaragua', 'Government of Honduras', 'Government of 

Nicaragua', 1, 1, 2, '5/1/1957', '5/7/1957', 1957, 1957, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (110, 'Mauritania, Morocco', 'Government of France', 'NLA', 1, 1, 1, 

'1/12/1957', '6/30/1958', 1957, 1958, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (111, 'Mauritania, Morocco', 'Government of Spain', 'NLA', 1, 1, 1, 

'11/23/1957', '6/30/1958', 1957, 1958, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (112, 'Oman', 'Government of Oman', 'State of Oman/Free Oman', 1, 1, 4, 

'7/31/1957', '8/26/1957', 1957, 1957, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (113, 'Iraq', 'Government of Iraq', 'Military faction (forces of Abdul Wahab 

al-Shawaf)', 2, 1, 3, '7/14/1958', '3/10/1959', 1958, 1959, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (114, 'Iraq', 'Government of Iraq', 'Military faction (forces of Abd as-Salam 

Arif) NCRC', 2, 2, 3, '2/8/1963', '11/20/1963', 1963, 1963, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (115, 'Iraq', 'Government of Iraq', 'SCIRI', 2, 1, 3, '8/1/1982', 

'12/31/1984', 1982, 1984, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (116, 'Iraq', 'Government of Iraq', 'SCIRI', 2, 1, 3, '12/31/1987', 

'12/31/1987', 1987, 1987, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (117, 'Iraq', 'Government of Iraq', 'SCIRI', 2, 1, 3, '12/31/1991', 

'12/31/1996', 1991, 1996, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (118, 'Lebanon', 'Government of Lebanon', 'Independent Nasserite 

Movement/Mourabitoun militia', 2, 2, 3, '5/15/1958', '7/31/1958', 1958, 1958, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (119, 'Lebanon', 'Government of Lebanon', 'LAA NSF', 2, 2, 3, '9/9/1975', 

'10/21/1976', 1975, 1976, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (120, 'Lebanon', 'Government of Lebanon', 'Lebanese Forces - Hobeika faction 

NUF', 2, 1, 3, '9/1/1982', '12/31/1986', 1982, 1986, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (121, 'Lebanon', 'Government of Lebanon', 'Forces of Michel Aoun', 2, 1, 4, 

'3/14/1989', '10/13/1990', 1989, 1990, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (122, 'Malaysia', 'Government of Malaysia', 'CPM', 2, 1, 4, '12/31/1958', 

'7/31/1960', 1958, 1960, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (123, 'Malaysia', 'Government of Malaysia', 'CPM', 2, 1, 3, '1/15/1974', 

'12/31/1975', 1974, 1975, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (124, 'Malaysia', 'Government of Malaysia', 'CPM', 2, 1, 3, '12/31/1981', 

'12/31/1981', 1981, 1981, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (125, 'Laos', 'Government of Laos', 'Pathet Lao', 2, 2, 4, '11/12/1959', 

'4/26/1961', 1959, 1961, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (126, 'Laos', 'Government of Laos', 'Pathet Lao', 2, 2, 4, '12/31/1963', 

'9/14/1973', 1963, 1973, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (127, 'Laos', 'Government of Laos', 'LRM', 2, 1, 3, '8/25/1989', '4/10/1990', 

1989, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (128, 'Angola', 'Government of Portugal', 'FNLA MPLA', 1, 1, 1, '2/4/1961', 

'10/21/1974', 1961, 1974, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (129, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'SSA SSNLO SURA', 1, 

2, 3, '11/22/1959', '12/31/1970', 1959, 1970, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (130, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'SSA', 1, 1, 3, 

'12/31/1972', '12/31/1973', 1972, 1973, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (131, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'MTA', 1, 1, 3, 

'8/31/1976', '12/31/1988', 1976, 1988, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (132, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'RCSS', 1, 1, 3, 

'7/16/1993', '9/24/2002', 1993, 2002, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (133, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'RCSS SSPP', 1, 1, 

3, '12/27/2005', '9/11/2011', 2005, 2011, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (134, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'State of 

Katanga', 1, 1, 3, '11/12/1961', '12/28/1962', 1961, 1962, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (135, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'Independent 

Mining State of South Kasai', 1, 1, 3, '8/31/1960', '12/31/1962', 1960, 1962, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (136, 'Ethiopia', 'Government of Ethiopia', 'Military faction (forces of 

Mengistu Neway)', 2, 1, 3, '12/17/1960', '12/17/1960', 1960, 1960, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (137, 'Ethiopia', 'Government of Ethiopia', 'EPRDF Military faction (Harar 

garrison)', 2, 2, 3, '6/2/1976', '6/2/1991', 1976, 1991, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (138, 'Ethiopia, Somalia', 'Government of Ethiopia', 'Government of Somalia', 

1, 1, 2, '2/13/1964', '3/30/1964', 1964, 1964, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (139, 'Ethiopia, Somalia', 'Government of Ethiopia', 'Government of Somalia', 

1, 2, 2, '7/13/1977', '3/23/1978', 1977, 1978, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (140, 'Ethiopia, Somalia', 'Government of Ethiopia', 'Government of Somalia', 

1, 1, 2, '5/31/1980', '12/3/1980', 1980, 1980, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (141, 'Nepal', 'Government of Nepal', 'NC', 2, 1, 3, '2/29/1960', 

'12/31/1962', 1960, 1962, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (142, 'Nepal', 'Government of Nepal', 'CPN-M', 2, 1, 3, '8/23/1996', 

'9/21/2006', 1996, 2006, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (143, 'France', 'Government of France', 'OAS', 2, 2, 3, '4/22/1961', 

'6/30/1962', 1961, 1962, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (144, 'Iraq', 'Government of Iraq', 'KDP', 1, 1, 3, '12/31/1961', '3/11/1970', 

1961, 1970, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (145, 'Iraq', 'Government of Iraq', 'PUK', 1, 1, 3, '7/31/1973', '12/31/1992', 

1973, 1992, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (146, 'Iraq', 'Government of Iraq', 'PUK', 1, 1, 3, '3/14/1995', '11/30/1996', 

1995, 1996, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (147, 'France, Tunisia', 'Government of France', 'Government of Tunisia', 1, 

2, 2, '7/20/1961', '7/22/1961', 1961, 1961, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (148, 'Brunei', 'Government of United Kingdom', 'North Kalimantan Liberation 

Army', 1, 1, 1, '12/8/1962', '12/31/1962', 1962, 1962, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (149, 'China, India', 'Government of China', 'Government of India', 1, 2, 2, 

'11/21/1962', '11/21/1962', 1962, 1962, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (150, 'China, India', 'Government of China', 'Government of India', 1, 1, 2, 

'1/31/1967', '12/31/1967', 1967, 1967, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (151, 'Ethiopia', 'Government of Ethiopia', 'EPLF', 1, 2, 3, '3/15/1964', 

'5/31/1991', 1964, 1991, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (152, 'Indonesia, Netherlands', 'Government of Indonesia', 'Government of 

Netherlands', 1, 1, 2, '1/15/1962', '9/21/1962', 1962, 1962, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (153, 'Venezuela', 'Government of Venezuela', 'Military faction (navy)', 2, 1, 

3, '6/3/1962', '12/31/1962', 1962, 1962, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (154, 'Venezuela', 'Government of Venezuela', 'Bandera Roja', 2, 1, 3, 

'4/30/1982', '10/4/1982', 1982, 1982, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (155, 'Venezuela', 'Government of Venezuela', 'Military faction (forces of 

Hugo Ch vez) ', 2, 1, 3, '2/4/1992', '11/29/1992', 1992, 1992, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (156, 'Algeria, Morocco', 'Government of Algeria', 'Government of Morocco', 1, 

1, 2, '10/8/1963', '11/4/1963', 1963, 1963, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (157, 'Guinea-Bissau', 'Government of Portugal', 'PAIGC', 1, 1, 1, 

'2/28/1963', '12/31/1973', 1963, 1973, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (158, 'Malaysia', 'Government of Malaysia', 'CCO', 1, 1, 4, '12/31/1963', 

'12/31/1966', 1963, 1966, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (159, 'Indonesia, Malaysia', 'Government of Indonesia', 'Government of 

Malaysia', 1, 1, 2, '12/31/1963', '8/11/1966', 1963, 1966, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (160, 'Sudan', 'Government of Sudan', 'SSLM', 1, 2, 3, '12/31/1963', 

'1/31/1972', 1963, 1972, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (161, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'CNL', 2, 2, 3, 

'1/31/1964', '12/31/1965', 1964, 1965, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (162, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'Military Faction 

(Forces of Jean Schramme)', 2, 1, 3, '7/5/1967', '11/5/1967', 1967, 1967, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (163, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'FLNC', 2, 1, 3, 

'4/30/1977', '6/15/1978', 1977, 1978, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (164, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'RCD', 2, 1, 4, 

'10/19/1996', '9/29/2001', 1996, 2001, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (165, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'CNDP', 2, 1, 3, 

'11/28/2006', '10/29/2008', 2006, 2008, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (166, 'Gabon', 'Government of Gabon', 'Military faction (forces loyal to L‚on 

MBa)', 2, 1, 4, '2/18/1964', '2/19/1964', 1964, 1964, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (167, 'Mozambique', 'Government of Portugal', 'Frelimo', 1, 1, 1, 

'11/19/1964', '9/7/1974', 1964, 1974, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (168, 'South Yemen', 'Government of United Kingdom', 'FLOSY', 1, 1, 1, 

'11/25/1964', '11/29/1967', 1964, 1967, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (169, 'Burundi', 'Government of Burundi', 'Military faction (forces loyal to 

Gervais Nyangoma)', 2, 1, 3, '10/19/1965', '10/19/1965', 1965, 1965, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (170, 'Burundi', 'Government of Burundi', 'Palipehutu', 2, 1, 3, '11/27/1991', 

'4/14/1992', 1991, 1992, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (171, 'Burundi', 'Government of Burundi', 'Palipehutu-FNL', 2, 1, 3, 

'10/18/1994', '9/26/2006', 1994, 2006, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (172, 'Burundi', 'Government of Burundi', 'Palipehutu-FNL', 2, 1, 3, 

'3/1/2008', '8/22/2008', 2008, 2008, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (173, 'Chad', 'Government of Chad', 'First Liberation Army', 2, 1, 4, 

'7/31/1966', '11/24/1972', 1966, 1972, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (174, 'Chad', 'Government of Chad', 'GUNT', 2, 1, 4, '2/18/1976', 

'12/31/1984', 1976, 1984, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (175, 'Chad', 'Government of Chad', 'CDR', 2, 2, 4, '12/31/1986', 

'11/22/1987', 1986, 1987, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (176, 'Chad', 'Government of Chad', 'CNR CSNPD FNT', 2, 1, 3, '3/3/1989', 

'12/31/1994', 1989, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (177, 'Chad', 'Government of Chad', 'MDJT', 2, 1, 3, '10/30/1997', 

'12/14/2003', 1997, 2003, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (178, 'Chad', 'Government of Chad', 'PFNR', 2, 1, 3, '12/18/2005', 

'4/28/2010', 2005, 2010, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (179, 'Dominican Republic', 'Government of Dominican Republic', 'Military 

faction (constitutionalists)', 2, 1, 3, '4/24/1965', '8/31/1965', 1965, 1965, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (180, 'Indonesia', 'Government of Indonesia', 'OPM', 1, 1, 3, '7/28/1965', 

'12/31/1965', 1965, 1965, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (181, 'Indonesia', 'Government of Indonesia', 'OPM', 1, 1, 3, '12/31/1967', 

'12/31/1969', 1967, 1969, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (182, 'Indonesia', 'Government of Indonesia', 'OPM', 1, 2, 3, '12/31/1976', 

'12/31/1978', 1976, 1978, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (183, 'Indonesia', 'Government of Indonesia', 'OPM', 1, 2, 3, '12/31/1981', 

'12/31/1981', 1981, 1981, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (184, 'Indonesia', 'Government of Indonesia', 'OPM', 1, 1, 3, '12/31/1984', 

'12/31/1984', 1984, 1984, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (185, 'Peru', 'Government of Peru', 'ELN MIR', 2, 1, 3, '8/3/1965', 

'12/31/1965', 1965, 1965, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (186, 'Peru', 'Government of Peru', 'Sendero Luminoso', 2, 1, 3, '8/22/1982', 

'12/31/1999', 1982, 1999, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (187, 'Peru', 'Government of Peru', 'Sendero Luminoso', 2, 1, 3, '11/14/2007', 

' 2010-12-30', 2007, 2-30, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (188, 'South Vietnam, Vietnam (North Vietnam)', 'Government of South Vietnam', 

'Government of Vietnam (North Vietnam)', 1, 2, 2, '12/31/1965', '4/30/1975', 1965, 1975, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (189, 'Cambodia (Kampuchea), Thailand', 'Government of Cambodia (Kampuchea)', 

'Government of Thailand', 1, 1, 2, '12/31/1977', '12/31/1978', 1977, 1978, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (190, 'Cambodia (Kampuchea), Thailand', 'Government of Cambodia (Kampuchea)', 

'Government of Thailand', 1, 1, 2, '4/28/2011', '5/2/2011', 2011, 2011, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (191, 'Ghana', 'Government of Ghana', 'NLC', 2, 1, 3, '2/24/1966', 

'2/24/1966', 1966, 1966, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (192, 'Ghana', 'Government of Ghana', 'Military faction (forces of Jerry John 

Rawlings)', 2, 1, 3, '12/31/1981', '12/31/1981', 1981, 1981, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (193, 'Ghana', 'Government of Ghana', 'Military faction (forces of Ekow Dennis 

and Edward Adjei-Ampofo) ', 2, 1, 3, '6/19/1983', '6/19/1983', 1983, 1983, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (194, 'India', 'Government of India', 'MNF', 1, 1, 3, '9/1/1966', 

'12/31/1968', 1966, 1968, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (195, 'Nigeria', 'Government of Nigeria', 'Military faction (forces of Patrick 

Nzeogwu)', 2, 1, 3, '1/15/1966', '7/29/1966', 1966, 1966, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (196, 'Nigeria', 'Government of Nigeria', 'Jamaatu Ahlis Sunna Liddaawati 

wal-Jihad', 2, 1, 3, '7/26/2009', '7/30/2009', 2009, 2009, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (197, 'South Africa', 'Government of South Africa', 'SWAPO', 1, 2, 3, 

'12/31/1966', '8/8/1988', 1966, 1988, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (198, 'Syria', 'Government of Syria', 'Military faction (forces loyal to 

Nureddin Atassi and Youssef Zeayen)', 2, 1, 3, '2/23/1966', '2/23/1966', 1966, 1966, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (199, 'Syria', 'Government of Syria', 'Muslim Brotherhood', 2, 2, 3, 

'6/16/1979', '2/2/1982', 1979, 1982, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (200, 'Cambodia (Kampuchea)', 'Government of Cambodia (Kampuchea)', 'KR', 2, 

2, 4, '5/31/1967', '4/17/1975', 1967, 1975, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (201, 'Cambodia (Kampuchea)', 'Government of Cambodia (Kampuchea)', 'KR', 2, 

1, 3, '12/30/1978', '10/26/1998', 1978, 1998, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (202, 'Egypt, Israel', 'Government of Egypt', 'Government of Israel', 1, 2, 2, 

'6/5/1967', '6/10/1967', 1967, 1967, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (203, 'Egypt, Israel', 'Government of Egypt', 'Government of Israel', 1, 1, 2, 

'3/6/1969', '8/7/1970', 1969, 1970, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (204, 'Egypt, Israel', 'Government of Egypt', 'Government of Israel', 1, 2, 2, 

'10/6/1973', '10/24/1973', 1973, 1973, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (205, 'Israel, Jordan', 'Government of Israel', 'Government of Jordan', 1, 2, 

2, '6/5/1967', '6/10/1967', 1967, 1967, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (206, 'Israel, Syria', 'Government of Israel', 'Government of Syria', 1, 2, 2, 

'6/5/1967', '6/10/1967', 1967, 1967, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (207, 'Israel, Syria', 'Government of Israel', 'Government of Syria', 1, 2, 2, 

'10/6/1973', '10/24/1973', 1973, 1973, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (208, 'Nigeria', 'Government of Nigeria', 'Republic of Biafra', 1, 2, 3, 

'7/6/1967', '1/12/1970', 1967, 1970, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (209, 'China, Myanmar (Burma)', 'Government of China', 'Government of Myanmar 

(Burma)', 1, 1, 2, '2/28/1969', '12/31/1969', 1969, 1969, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (210, 'China, Russia (Soviet Union)', 'Government of China', 'Government of 

Russia (Soviet Union)', 1, 1, 2, '3/2/1969', '12/31/1969', 1969, 1969, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (211, 'El Salvador, Honduras', 'Government of El Salvador', 'Government of 

Honduras', 1, 2, 2, '7/3/1969', '7/18/1969', 1969, 1969, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (212, 'Guinea', 'Government of Guinea', 'RFDG', 2, 1, 3, '9/17/2000', 

'7/19/2001', 2000, 2001, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (213, 'Philippines', 'Government of Philippines', 'MILF MNLF', 1, 1, 3, 

'8/20/1970', '12/31/1990', 1970, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (214, 'Sudan', 'Government of Sudan', 'Sudanese Communist Party', 2, 1, 3, 

'7/22/1971', '7/22/1971', 1971, 1971, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (215, 'Sudan', 'Government of Sudan', 'Islamic Charter Front', 2, 1, 3, 

'7/2/1976', '7/2/1976', 1976, 1976, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (216, 'Madagascar (Malagasy)', 'Government of Madagascar', 'Monima ', 2, 1, 3, 

'4/30/1971', '4/30/1971', 1971, 1971, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (217, 'Morocco', 'Government of Morocco', 'Military faction (forces of Mohamed 

Madbouh)', 2, 1, 3, '7/10/1971', '7/11/1971', 1971, 1971, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (218, 'Pakistan', 'Government of Pakistan', 'Mukti Bahini', 1, 2, 3, 

'3/26/1971', '12/16/1971', 1971, 1971, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (219, 'Sri Lanka', 'Government of Sri Lanka', 'JVP', 2, 2, 3, '4/30/1971', 

'6/9/1971', 1971, 1971, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (220, 'Sri Lanka', 'Government of Sri Lanka', 'JVP', 2, 1, 3, '2/13/1989', 

'2/23/1990', 1989, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (221, 'Uganda', 'Government of Uganda', 'Kikosi Maalum', 2, 1, 3, '1/29/1971', 

'9/20/1972', 1971, 1972, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (222, 'Uganda', 'Government of Uganda', 'Military faction (forces of Charles 

Arube)', 2, 1, 3, '3/23/1974', '3/23/1974', 1974, 1974, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (223, 'Uganda', 'Government of Uganda', 'UPA', 2, 1, 3, '1/22/1979', 

'8/9/1992', 1979, 1992, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (224, 'Uganda', 'Government of Uganda', 'ADF LRA', 2, 1, 4, '2/21/1994', 

'11/3/2011', 1994, 2011, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (225, 'United Kingdom', 'Government of United Kingdom', 'PIRA', 1, 1, 3, 

'8/31/1971', '11/26/1991', 1971, 1991, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (226, 'United Kingdom', 'Government of United Kingdom', 'RIRA', 1, 1, 3, 

'8/15/1998', '8/15/1998', 1998, 1998, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (227, 'El Salvador', 'Government of El Salvador', 'Military faction (forces of 

Benjamin Mejia)', 2, 1, 3, '3/25/1972', '3/25/1972', 1972, 1972, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (228, 'El Salvador', 'Government of El Salvador', 'FMLN', 2, 1, 3, 

'9/14/1979', '12/31/1991', 1979, 1991, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (229, 'Oman', 'Government of Oman', 'PFLO', 2, 1, 4, '12/31/1969', 

'12/11/1975', 1969, 1975, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (230, 'Zimbabwe (Rhodesia)', 'Government of Zimbabwe (Rhodesia)', 'ZAPU', 2, 

1, 3, '9/5/1967', '12/31/1968', 1967, 1968, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (231, 'Zimbabwe (Rhodesia)', 'Government of Zimbabwe (Rhodesia)', 'PF', 2, 2, 

3, '4/4/1973', '12/21/1979', 1973, 1979, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (232, 'Uruguay', 'Government of Uruguay', 'MLN/Tupamaros', 2, 1, 3, 

'4/30/1972', '12/31/1972', 1972, 1972, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (233, 'South Yemen, Yemen (North Yemen)', 'Government of South Yemen', 

'Government of Yemen (North Yemen)', 3, 1, 2, '2/21/1972', '10/19/1972', 1972, 1972, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (234, 'South Yemen, Yemen (North Yemen)', 'Government of South Yemen', 

'Government of Yemen (North Yemen)', 3, 1, 2, '3/31/1979', '3/30/1979', 1979, 1979, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (235, 'Chile', 'Government of Chile', 'Military faction (forces of Augusto 

Pinochet Toribio Merino and Leigh Guzman) ', 2, 1, 3, '9/11/1973', '9/11/1973', 1973, 1973, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (236, 'Bangladesh', 'Government of Bangladesh', 'JSS/SB', 1, 1, 3, 

'2/28/1975', '12/31/1991', 1975, 1991, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (237, 'Cyprus, Turkey', 'Government of Cyprus', 'Government of Turkey', 1, 2, 

2, '6/20/1974', '8/16/1974', 1974, 1974, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (238, 'Iran, Iraq', 'Government of Iran', 'Government of Iraq', 3, 1, 2, 

'2/10/1974', '12/31/1974', 1974, 1974, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (239, 'Iran, Iraq', 'Government of Iran', 'Government of Iraq', 3, 2, 2, 

'9/23/1980', '8/20/1988', 1980, 1988, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (240, 'Pakistan', 'Government of Pakistan', 'BLF', 1, 1, 3, '12/31/1974', 

'7/5/1977', 1974, 1977, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (241, 'Pakistan', 'Government of Pakistan', 'BLA', 1, 1, 3, '8/1/2004', 

'12/25/2004', 2004, 2004, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (242, 'Pakistan', 'Government of Pakistan', 'BLA BRA', 1, 1, 3, '3/26/2006', 

'10/20/2009', 2006, 2009, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (243, 'Eritrea', 'Government of Eritrea', 'EIJM - AS', 2, 1, 3, '6/30/1997', 

'6/30/1997', 1997, 1997, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (244, 'Eritrea', 'Government of Eritrea', 'EIJM - AS', 2, 1, 3, '4/4/1999', 

'11/29/1999', 1999, 1999, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (245, 'Eritrea', 'Government of Eritrea', 'EIJM - AS', 2, 1, 3, '7/17/2003', 

'8/9/2003', 2003, 2003, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (246, 'Angola', 'Government of Angola', 'UNITA', 2, 1, 3, '11/11/1975', 

'12/24/1995', 1975, 1995, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (247, 'Angola', 'Government of Angola', 'UNITA', 2, 1, 4, '5/2/1998', 

'4/1/2002', 1998, 2002, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (248, 'Cambodia (Kampuchea), Vietnam (North Vietnam)', 'Government of Cambodia 

(Kampuchea)', 'Government of Vietnam (North Vietnam)', 1, 1, 2, '12/31/1975', '12/31/1977', 1975, 1977, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (249, 'Ethiopia', 'Government of Ethiopia', 'Ogaden Liberation Front', 1, 1, 

3, '1/11/1964', '12/31/1964', 1964, 1964, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (250, 'Ethiopia', 'Government of Ethiopia', 'WSLF', 1, 1, 4, '10/31/1976', 

'12/31/1983', 1976, 1983, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (251, 'Ethiopia', 'Government of Ethiopia', 'ONLF', 1, 1, 3, '10/13/1993', 

'8/27/1994', 1993, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (252, 'Ethiopia', 'Government of Ethiopia', 'AIAI ONLF', 1, 1, 3, '8/9/1996', 

'12/31/1996', 1996, 1996, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (253, 'Indonesia', 'Government of Indonesia', 'Fretilin', 1, 1, 3, 

'12/7/1975', '12/31/1989', 1975, 1989, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (254, 'Indonesia', 'Government of Indonesia', 'Fretilin', 1, 1, 3, 

'12/15/1992', '12/31/1992', 1992, 1992, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (255, 'Indonesia', 'Government of Indonesia', 'Fretilin', 1, 1, 3, 

'5/31/1997', '12/31/1998', 1997, 1998, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (256, 'Morocco', 'Government of Morocco', 'POLISARIO', 1, 1, 3, '11/4/1975', 

'11/16/1989', 1975, 1989, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (257, 'Mozambique', 'Government of Mozambique', 'Renamo', 2, 1, 3, 

'12/31/1977', '10/19/1992', 1977, 1992, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (258, 'Afghanistan', 'Government of Afghanistan', 'UIFSA', 2, 2, 4, 

'4/27/1978', '12/7/2001', 1978, 2001, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (259, 'China, Vietnam (North Vietnam)', 'Government of China', 'Government of 

Vietnam (North Vietnam)', 1, 1, 2, '1/20/1974', '12/31/1974', 1974, 1974, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (260, 'China, Vietnam (North Vietnam)', 'Government of China', 'Government of 

Vietnam (North Vietnam)', 1, 1, 2, '12/31/1978', '12/31/1981', 1978, 1981, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (261, 'China, Vietnam (North Vietnam)', 'Government of China', 'Government of 

Vietnam (North Vietnam)', 1, 1, 2, '12/31/1983', '12/31/1984', 1983, 1984, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (262, 'China, Vietnam (North Vietnam)', 'Government of China', 'Government of 

Vietnam (North Vietnam)', 1, 1, 2, '12/31/1986', '12/31/1988', 1986, 1988, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (263, 'India', 'Government of India', 'TNV', 1, 1, 3, '12/31/1979', 

'8/12/1988', 1979, 1988, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (264, 'India', 'Government of India', 'ATTF', 1, 1, 3, '10/12/1992', 

'8/23/1993', 1992, 1993, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (265, 'India', 'Government of India', 'NLFT', 1, 1, 3, '10/31/1995', 

'12/31/1995', 1995, 1995, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (266, 'India', 'Government of India', 'NLFT', 1, 1, 3, '6/10/1997', 

'12/31/2004', 1997, 2004, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (267, 'India', 'Government of India', 'NLFT', 1, 1, 3, '11/7/2006', 

'12/31/2006', 2006, 2006, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (268, 'Nicaragua', 'Government of Nicaragua', 'FSLN', 2, 2, 3, '10/10/1977', 

'7/19/1979', 1977, 1979, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (269, 'Nicaragua', 'Government of Nicaragua', 'Contras/FDN', 2, 1, 3, 

'4/17/1982', '4/19/1990', 1982, 1990, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (270, 'Somalia', 'Government of Somalia', 'SNM SSDF', 2, 1, 3, '12/31/1982', 

'12/31/1984', 1982, 1984, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (271, 'Somalia', 'Government of Somalia', 'USC/SNA', 2, 1, 3, '3/3/1986', 

'12/20/1996', 1986, 1996, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (272, 'Somalia', 'Government of Somalia', 'SRRC', 2, 1, 3, '5/12/2001', 

'10/28/2002', 2001, 2002, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (273, 'Iran', 'Government of Iran', 'MEK', 2, 2, 3, '12/31/1979', 

'12/31/1982', 1979, 1982, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (274, 'Iran', 'Government of Iran', 'MEK', 2, 1, 3, '12/31/1986', 

'12/31/1988', 1986, 1988, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (275, 'Iran', 'Government of Iran', 'MEK', 2, 1, 3, '4/3/1991', '12/31/1993', 

1991, 1993, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (276, 'Iran', 'Government of Iran', 'MEK', 2, 1, 3, '2/18/1997', '12/31/1997', 

1997, 1997, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (277, 'Iran', 'Government of Iran', 'MEK', 2, 1, 3, '11/25/1999', 

'12/31/2001', 1999, 2001, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (278, 'Iran', 'Government of Iran', 'PJAK', 2, 1, 3, '8/7/2005', '12/27/2011', 

2005, 2011, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (279, 'Iran', 'Government of Iran', 'APCO', 1, 1, 3, '5/31/1979', 

'12/31/1980', 1979, 1980, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (280, 'Saudi Arabia', 'Government of Saudi Arabia', 'JSM', 2, 1, 3, 

'11/20/1979', '12/4/1979', 1979, 1979, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (281, 'Liberia', 'Government of Liberia', 'Military faction (forces of Samuel 

Doe)', 2, 1, 3, '4/12/1980', '4/14/1980', 1980, 1980, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (282, 'Liberia', 'Government of Liberia', 'INPFL NPFL', 2, 1, 3, '12/26/1989', 

'12/31/1990', 1989, 1990, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (283, 'Liberia', 'Government of Liberia', 'LURD MODEL', 2, 2, 3, '5/31/2000', 

'11/21/2003', 2000, 2003, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (284, 'Spain', 'Government of Spain', 'ETA', 1, 1, 3, '10/22/1978', 

'12/29/1982', 1978, 1982, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (285, 'Spain', 'Government of Spain', 'ETA', 1, 1, 3, '12/23/1985', 

'12/31/1987', 1985, 1987, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (286, 'Spain', 'Government of Spain', 'ETA', 1, 1, 3, '6/28/1991', 

'12/13/1991', 1991, 1991, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (287, 'Tunisia', 'Government of Tunisia', 'R‚sistance Arm‚e Tunisienne', 2, 1, 

3, '1/28/1980', '1/28/1980', 1980, 1980, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (288, 'Gambia', 'Government of Gambia', 'NRC', 2, 1, 4, '7/30/1981', 

'8/5/1981', 1981, 1981, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (289, 'South Africa', 'Government of South Africa', 'ANC', 2, 1, 3, 

'8/7/1981', '12/31/1983', 1981, 1983, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (290, 'South Africa', 'Government of South Africa', 'ANC', 2, 1, 3, 

'6/26/1985', '12/31/1988', 1985, 1988, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (291, 'Argentina, United Kingdom', 'Government of Argentina', 'Government of 

United Kingdom', 1, 1, 2, '5/2/1982', '6/14/1982', 1982, 1982, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (292, 'India', 'Government of India', 'PLA', 1, 1, 3, '7/31/1982', 

'12/31/1988', 1982, 1988, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (293, 'India', 'Government of India', 'PLA UNLF', 1, 1, 3, '12/31/1992', 

'12/31/2000', 1992, 2000, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (294, 'India', 'Government of India', 'KCP PREPAK UNLF ', 1, 1, 3, 

'10/23/2003', '12/24/2009', 2003, 2009, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (295, 'Kenya', 'Government of Kenya', 'Military faction (forces of Hezekiah 

Ochuka)', 2, 1, 3, '8/1/1982', '8/21/1982', 1982, 1982, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (296, 'Chad, Nigeria', 'Government of Chad', 'Government of Nigeria', 1, 1, 2, 

'4/24/1983', '11/1/1983', 1983, 1983, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (297, 'Grenada, United States of America', 'Government of Grenada', 

'Government of United States of America', 2, 1, 2, '10/25/1983', '10/27/1983', 1983, 1983, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (298, 'India', 'Government of India', 'Sikh insurgents', 1, 1, 3, 

'12/31/1983', '12/31/1993', 1983, 1993, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (299, 'Sri Lanka', 'Government of Sri Lanka', 'LTTE', 1, 2, 3, '9/1/1984', 

'12/12/2001', 1984, 2001, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (300, 'Sri Lanka', 'Government of Sri Lanka', 'LTTE', 1, 1, 3, '6/14/2003', 

'8/9/2003', 2003, 2003, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (301, 'Sri Lanka', 'Government of Sri Lanka', 'LTTE', 1, 2, 3, '12/2/2005', 

'7/4/2009', 2005, 2009, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (302, 'Cameroon', 'Government of Cameroon', 'UPC', 2, 1, 3, '1/31/1960', 

'12/31/1961', 1960, 1961, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (303, 'Cameroon', 'Government of Cameroon', 'Military faction (forces of 

Ibrahim Saleh)', 2, 1, 3, '4/6/1984', '4/9/1984', 1984, 1984, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (304, 'Burkina Faso, Mali', 'Government of Burkina Faso', 'Government of 

Mali', 1, 1, 2, '12/26/1985', '12/31/1985', 1985, 1985, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (305, 'Laos, Thailand', 'Government of Laos', 'Government of Thailand', 1, 1, 

2, '12/31/1986', '2/17/1988', 1986, 1988, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (306, 'Suriname', 'Government of Surinam', 'SLA', 2, 1, 3, '10/12/1987', 

'12/31/1987', 1987, 1987, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (307, 'Togo', 'Government of Togo', 'MTD', 2, 1, 3, '9/23/1986', '9/24/1986', 

1986, 1986, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (308, 'South Yemen', 'Government of South Yemen', 'Yemenite Socialist Party - 

Abdul Fattah Ismail faction', 2, 2, 3, '1/13/1986', '1/20/1986', 1986, 1986, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (309, 'Burkina Faso', 'Government of Burkina Faso', 'Popular Front', 2, 1, 3, 

'10/15/1987', '10/15/1987', 1987, 1987, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (310, 'Chad, Libya', 'Government of Chad', 'Government of Libya', 1, 2, 2, 

'8/8/1987', '9/12/1987', 1987, 1987, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (311, 'Comoros', 'Government of Comoros', 'Presidential guard', 2, 1, 3, 

'11/29/1989', '11/29/1989', 1989, 1989, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (312, 'Ethiopia', 'Government of Ethiopia', 'ALF', 1, 1, 3, '6/30/1975', 

'12/31/1976', 1975, 1976, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (313, 'Ethiopia', 'Government of Ethiopia', 'ARDUF', 1, 1, 3, '12/31/1996', 

'12/31/1996', 1996, 1996, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (314, 'India', 'Government of India', 'ULFA', 1, 1, 3, '12/31/1990', 

'12/31/1991', 1990, 1991, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (315, 'India', 'Government of India', 'ULFA', 1, 1, 3, '12/31/1994', 

'10/16/2010', 1994, 2010, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (316, 'Indonesia', 'Government of Indonesia', 'GAM', 1, 1, 3, '6/6/1990', 

'12/31/1991', 1990, 1991, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (317, 'Indonesia', 'Government of Indonesia', 'GAM', 1, 1, 3, '6/19/1999', 

'10/12/2005', 1999, 2005, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (318, 'Panama', 'Government of Panama', 'Military faction (forces of Mois‚s 

Giroldi) ', 2, 1, 3, '10/3/1989', '10/3/1989', 1989, 1989, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (319, 'Panama, United States of America', 'Government of Panama', 'Government 

of United States of America', 2, 1, 2, '12/20/1989', '12/31/1989', 1989, 1989, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (320, 'Papua New Guinea', 'Government of Papua New Guinea', 'BRA', 1, 1, 3, 

'2/7/1990', '10/1/1990', 1990, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (321, 'Papua New Guinea', 'Government of Papua New Guinea', 'BRA', 1, 1, 3, 

'8/2/1992', '11/28/1996', 1992, 1996, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (322, 'Rumania', 'Government of Rumania', 'NSF', 2, 1, 3, '12/22/1989', 

'12/23/1989', 1989, 1989, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (323, 'Iraq, Kuwait', 'Government of Iraq', 'Government of Kuwait', 1, 2, 2, 

'8/2/1990', '3/2/1991', 1990, 1991, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (324, 'Mali', 'Government of Mali', 'MPA', 1, 1, 3, '7/21/1990', '9/4/1990', 

1990, 1990, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (325, 'Mali', 'Government of Mali', 'FIAA', 1, 1, 3, '10/4/1994', 

'12/18/1994', 1994, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (326, 'Mali', 'Government of Mali', 'ATNMC', 1, 1, 3, '8/31/2007', 

'1/22/2009', 2007, 2009, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (327, 'Mali', 'Government of Mali', 'MNLA', 1, 1, 3, '1/24/2012', '3/31/2012', 

2012, 2012, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (328, 'Niger', 'Government of Niger', 'CRA', 1, 1, 3, '5/16/1994', 

'9/26/1994', 1994, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (329, 'Rwanda', 'Government of Rwanda', 'FPR', 2, 2, 3, '10/3/1990', 

'7/4/1994', 1990, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (330, 'Rwanda', 'Government of Rwanda', 'FDLR', 2, 1, 3, '7/12/1996', 

'3/1/2002', 1996, 2002, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (331, 'Rwanda', 'Government of Rwanda', 'FDLR', 2, 1, 4, '2/16/2009', 

'12/2/2012', 2009, 2012, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (332, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '8/31/1990', 

'12/31/1990', 1990, 1990, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (333, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '9/1/1992', 

'11/16/1993', 1992, 1993, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (334, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '4/27/1995', 

'12/1/1995', 1995, 1995, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (335, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '3/18/1997', 

'11/2/1998', 1997, 1998, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (336, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '4/11/2000', 

'12/7/2001', 2000, 2001, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (337, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '1/20/2003', 

'10/11/2003', 2003, 2003, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (338, 'Senegal', 'Government of Senegal', 'MFDC', 1, 1, 3, '12/20/2011', 

'12/30/2011', 2011, 2011, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (339, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 

'Republic of Armenia', 1, 1, 3, '8/22/1990', '12/21/1991', 1990, 1991, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (340, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'APF', 

1, 1, 3, '1/19/1990', '1/20/1990', 1990, 1990, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (341, 'Trinidad and Tobago', 'Government of Trinidad and Tobago', 'Jamaat al-

Muslimeen', 2, 1, 3, '7/30/1990', '8/1/1990', 1990, 1990, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (342, 'Djibouti', 'Government of Djibouti', 'FRUD', 2, 1, 3, '11/13/1991', 

'3/4/1994', 1991, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (343, 'Djibouti', 'Government of Djibouti', 'FRUD - AD', 2, 1, 3, '7/24/1999', 

'8/31/1999', 1999, 1999, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (344, 'Georgia', 'Government of Georgia', 'Zviadists', 2, 1, 3, '12/28/1991', 

'12/31/1993', 1991, 1993, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (345, 'Haiti', 'Government of Haiti', 'Military faction (forces of Himmler 

Rebu and Guy Francois) ', 2, 1, 3, '4/8/1989', '4/11/1989', 1989, 1989, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (346, 'Haiti', 'Government of Haiti', 'Military faction (forces of Raol C

‚dras) ', 2, 1, 3, '10/2/1991', '10/2/1991', 1991, 1991, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (347, 'Haiti', 'Government of Haiti', 'FLRN OP Lavalas (ChimŠres) ', 2, 1, 3, 

'2/9/2004', '12/31/2004', 2004, 2004, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (348, 'Sierra Leone', 'Government of Sierra Leone', 'RUF', 2, 1, 3, 

'4/1/1991', '12/20/2001', 1991, 2001, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (349, 'Turkey', 'Government of Turkey', 'Devrimci Sol', 2, 1, 3, '7/13/1991', 

'12/31/1992', 1991, 1992, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (350, 'Turkey', 'Government of Turkey', 'MKP', 2, 1, 3, '6/18/2005', 

'10/17/2005', 2005, 2005, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (351, 'Serbia (Yugoslavia)', 'Government of Serbia (Yugoslavia)', 'Republic of 

Slovenia', 1, 1, 3, '6/28/1991', '7/12/1991', 1991, 1991, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (352, 'Serbia (Yugoslavia)', 'Government of Serbia (Yugoslavia)', 'Croatian 

irregulars Republic of Croatia', 1, 2, 3, '7/27/1991', '12/31/1991', 1991, 1991, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (353, 'Angola', 'Government of Angola', 'FLEC-R', 1, 1, 3, '6/3/1991', 

'11/10/1991', 1991, 1991, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (354, 'Angola', 'Government of Angola', 'FLEC-FAC FLEC-R', 1, 1, 3, 

'12/31/1994', '12/31/1994', 1994, 1994, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (355, 'Angola', 'Government of Angola', 'FLEC-FAC', 1, 1, 3, '5/22/1996', 

'12/31/1998', 1996, 1998, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (356, 'Angola', 'Government of Angola', 'FLEC-FAC FLEC-R', 1, 1, 3, 

'11/12/2002', '12/31/2002', 2002, 2002, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (357, 'Angola', 'Government of Angola', 'FLEC-FAC', 1, 1, 3, '2/10/2004', 

'4/24/2004', 2004, 2004, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (358, 'Angola', 'Government of Angola', 'FLEC-FAC', 1, 1, 3, '12/31/2007', 

'12/31/2007', 2007, 2007, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (359, 'Angola', 'Government of Angola', 'FLEC-FAC', 1, 1, 3, '12/31/2009', 

'12/31/2009', 2009, 2009, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (360, 'Azerbaijan', 'Government of Azerbaijan', 'Republic of Nagorno-

Karabakh', 1, 2, 3, '12/30/1991', '7/27/1994', 1991, 1994, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (361, 'Azerbaijan', 'Government of Azerbaijan', 'Republic of Nagorno-

Karabakh', 1, 1, 4, '10/9/2005', '12/5/2005', 2005, 2005, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (362, 'Azerbaijan', 'Government of Azerbaijan', 'Republic of Nagorno-

Karabakh', 1, 1, 4, '9/11/2012', '12/31/2012', 2012, 2012, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (363, 'Bosnia-Herzegovina', 'Government of Bosnia-Herzegovina', 'Serbian 

irregulars Serbian Republic of Bosnia-Herzegovina', 1, 2, 3, '4/30/1992', '11/21/1995', 1992, 1995, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (364, 'Croatia', 'Government of Croatia', 'Serbian Republic of Krajina', 1, 1, 

4, '5/17/1992', '12/23/1993', 1992, 1993, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (365, 'Croatia', 'Government of Croatia', 'Serbian Republic of Krajina', 1, 1, 

3, '5/1/1995', '11/12/1995', 1995, 1995, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (366, 'Egypt', 'Government of Egypt', 'al-Gamaa al-Islamiyya', 2, 1, 3, 

'3/10/1993', '11/2/1998', 1993, 1998, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (367, 'Georgia', 'Government of Georgia', 'Republic of Abkhazia', 1, 2, 3, 

'8/18/1992', '12/1/1993', 1992, 1993, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (368, 'Georgia', 'Government of Georgia', 'Republic of South Ossetia', 1, 1, 

3, '6/8/1992', '7/14/1992', 1992, 1992, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (369, 'Georgia', 'Government of Georgia', 'Republic of South Ossetia', 1, 1, 

3, '8/19/2004', '8/19/2004', 2004, 2004, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (370, 'Georgia', 'Government of Georgia', 'Republic of South Ossetia', 1, 1, 

4, '8/8/2008', '8/14/2008', 2008, 2008, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (371, 'Moldova', 'Government of Moldova', 'PMR', 1, 1, 3, '3/17/1992', 

'7/21/1992', 1992, 1992, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (372, 'Tajikistan', 'Government of Tajikistan', 'Forces of Khudoberdiyev UTO', 

2, 1, 3, '5/10/1992', '11/16/1998', 1992, 1998, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (373, 'Tajikistan', 'Government of Tajikistan', 'Forces of Mullo Abdullo', 2, 

1, 3, '9/6/2000', '9/6/2000', 2000, 2000, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (374, 'Tajikistan', 'Government of Tajikistan', 'IMU', 2, 1, 3, '9/9/2010', 

'7/25/2011', 2010, 2011, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (375, 'Azerbaijan', 'Government of Azerbaijan', 'Military faction (forces of 

Suret Husseinov)', 2, 1, 3, '6/6/1993', '6/18/1993', 1993, 1993, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (376, 'Azerbaijan', 'Government of Azerbaijan', 'OPON Forces', 2, 1, 3, 

'3/17/1995', '3/17/1995', 1995, 1995, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (377, 'Bosnia-Herzegovina', 'Government of Bosnia-Herzegovina', 'Autonomous 

Province of Western Bosnia', 1, 1, 3, '10/5/1993', '8/7/1995', 1993, 1995, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (378, 'Bosnia-Herzegovina', 'Government of Bosnia-Herzegovina', 'Croatian 

irregulars Croatian Republic of Bosnia-Herzegovina', 1, 1, 3, '1/15/1993', '3/1/1994', 1993, 1994, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (379, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 

'Parliamentary Forces', 2, 1, 3, '10/3/1993', '10/4/1993', 1993, 1993, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (380, 'Mexico', 'Government of Mexico', 'EZLN', 2, 1, 3, '1/1/1994', 

'1/12/1994', 1994, 1994, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (381, 'Mexico', 'Government of Mexico', 'EPR', 2, 1, 3, '9/16/1996', 

'12/31/1996', 1996, 1996, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (382, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'Chechen 

Republic of Ichkeria', 1, 2, 3, '11/26/1994', '8/31/1996', 1994, 1996, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (383, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'Chechen 

Republic of Ichkeria', 1, 1, 3, '7/18/1999', '10/7/2007', 1999, 2007, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (384, 'Yemen (North Yemen)', 'Government of Yemen (North Yemen)', 'Democratic 

Republic of Yemen', 1, 2, 3, '4/28/1994', '7/4/1994', 1994, 1994, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (385, 'Ecuador, Peru', 'Government of Ecuador', 'Government of Peru', 1, 1, 2, 

'1/30/1995', '2/17/1995', 1995, 1995, 5);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (386, 'Pakistan', 'Government of Pakistan', 'MQM', 2, 1, 3, '2/11/1990', 

'2/11/1990', 1990, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (387, 'Pakistan', 'Government of Pakistan', 'MQM', 2, 1, 3, '5/3/1994', 

'9/4/1996', 1994, 1996, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (388, 'Cameroon, Nigeria', 'Government of Cameroon', 'Government of Nigeria', 

1, 1, 2, '5/7/1996', '5/7/1996', 1996, 1996, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (389, 'Niger', 'Government of Niger', 'FDR', 1, 1, 3, '7/10/1995', 

'7/10/1995', 1995, 1995, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (390, 'Comoros', 'Government of Comoros', 'MPA/Republic of Anjouan', 1, 1, 3, 

'9/5/1997', '9/5/1997', 1997, 1997, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (391, 'Congo', 'Government of Congo', 'Ninjas', 2, 1, 3, '11/11/1993', 

'12/27/1993', 1993, 1993, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (392, 'Congo', 'Government of Congo', 'Cocoyes Ninjas Ntsiloulous', 2, 1, 4, 

'6/6/1997', '12/31/1999', 1997, 1999, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (393, 'Congo', 'Government of Congo', 'Ntsiloulous', 2, 1, 4, '4/10/2002', 

'12/31/2002', 2002, 2002, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (394, 'Eritrea, Ethiopia', 'Government of Eritrea', 'Government of Ethiopia', 

1, 2, 2, '5/31/1998', '6/18/2000', 1998, 2000, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (395, 'Guinea-Bissau', 'Government of Guinea-Bissau', 'Military Junta for the 

Consolidation of Democracy Peace and Justice', 2, 1, 4, '6/7/1998', '5/7/1999', 1998, 1999, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (396, 'Lesotho', 'Government of Lesotho', 'Military faction', 2, 1, 4, 

'9/22/1998', '9/28/1998', 1998, 1998, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (397, 'Serbia (Yugoslavia)', 'Government of Serbia (Yugoslavia)', 'UCK', 1, 2, 

4, '3/6/1998', '6/30/1999', 1998, 1999, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (398, 'Ethiopia', 'Government of Ethiopia', 'OLF', 1, 1, 3, '12/31/1977', 

'12/31/1978', 1977, 1978, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (399, 'Ethiopia', 'Government of Ethiopia', 'OLF', 1, 2, 3, '12/31/1980', 

'12/31/1981', 1980, 1981, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (400, 'Ethiopia', 'Government of Ethiopia', 'OLF', 1, 1, 3, '7/31/1983', 

'6/30/1992', 1983, 1992, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (401, 'Ethiopia', 'Government of Ethiopia', 'OLF', 1, 1, 3, '12/31/1994', 

'12/31/1995', 1994, 1995, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (402, 'Russia (Soviet Union)', 'Government of Russia (Soviet Union)', 'Wahhabi 

movement of the Buinaksk district', 1, 1, 3, '9/2/1999', '9/24/1999', 1999, 1999, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (403, 'Uzbekistan', 'Government of Uzbekistan', 'IMU', 2, 1, 4, '3/30/1999', 

'10/1/2000', 1999, 2000, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (404, 'Uzbekistan', 'Government of Uzbekistan', 'JIG', 2, 1, 3, '3/30/2004', 

'7/30/2004', 2004, 2004, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (405, 'Central African Republic', 'Government of Central African Republic', 

'Forces of Francois Bozize', 2, 1, 3, '6/1/2001', '12/19/2002', 2001, 2002, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (406, 'Central African Republic', 'Government of Central African Republic', 

'UFDR', 2, 1, 4, '11/28/2006', '12/1/2006', 2006, 2006, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (407, 'Macedonia, FYR', 'Government of Macedonia FYR', 'UCK', 2, 1, 3, 

'5/31/2001', '8/13/2001', 2001, 2001, 1);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (408, 'Ivory Coast', 'Government of Ivory Coast', 'FRCI', 2, 1, 3, 

'9/20/2002', '11/9/2004', 2002, 2004, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (409, 'Ivory Coast', 'Government of Ivory Coast', 'FDSI-CI', 2, 1, 3, 

'3/13/2011', '4/27/2011', 2011, 2011, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (410, 'Australia, Iraq, United Kingdom, United States of America', 'Government 

of Australia Government of United Kingdom Government of United States of America', 'Government of Iraq', 2, 2, 2, 

'3/22/2003', '4/9/2003', 2003, 2003, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (411, 'India', 'Government of India', 'ABSU', 1, 1, 3, '10/10/1989', 

'12/31/1990', 1989, 1990, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (412, 'India', 'Government of India', 'NDFB', 1, 1, 3, '12/31/1993', 

'10/15/2004', 1993, 2004, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (413, 'India', 'Government of India', 'NDFB NDFB - RD', 1, 1, 3, '6/12/2009', 

'11/8/2010', 2009, 2010, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (414, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'UWSA', 1, 1, 3, 

'3/16/1997', '7/15/1997', 1997, 1997, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (415, 'Nigeria', 'Government of Nigeria', 'Ahlul Sunnah Jamaa', 1, 1, 3, 

'9/23/2004', '10/8/2004', 2004, 2004, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (416, 'Nigeria', 'Government of Nigeria', 'NDPVF', 1, 1, 3, '6/4/2004', 

'9/20/2004', 2004, 2004, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (417, 'Israel', 'Government of Israel', 'Hezbollah', 1, 1, 3, '7/10/1990', 

'12/31/1999', 1990, 1999, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (418, 'Israel', 'Government of Israel', 'Hezbollah', 1, 1, 3, '7/13/2006', 

'8/14/2006', 2006, 2006, 2);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (419, 'Tanzania, Uganda', 'Government of Tanzania', 'Government of Uganda', 1, 

1, 2, '11/15/1978', '11/16/1978', 1978, 1978, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (420, 'Mauritania', 'Government of Mauritania', 'POLISARIO', 1, 1, 3, 

'12/19/1975', '12/31/1978', 1975, 1978, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (421, 'DR Congo (Zaire)', 'Government of DR Congo (Zaire) ', 'BDK', 1, 1, 3, 

'2/1/2007', '3/31/2008', 2007, 2008, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (422, 'Niger', 'Government of Niger', 'FLAA', 2, 1, 3, '12/31/1991', 

'12/18/1992', 1991, 1992, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (423, 'Niger', 'Government of Niger', 'UFRA', 2, 1, 3, '10/19/1997', 

'11/28/1997', 1997, 1997, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (424, 'Niger', 'Government of Niger', 'MNJ', 2, 1, 3, '4/30/2007', 

'11/16/2008', 2007, 2008, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (425, 'Afghanistan, Russia (Soviet Union)', 'Government of Afghanistan', 

'Government of Russia (Soviet Union)', 2, 1, 2, '12/27/1979', '12/28/1979', 1979, 1979, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (426, 'India', 'Government of India', 'PULF', 1, 1, 3, '11/9/2008', 

'12/31/2008', 2008, 2008, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (427, 'Djibouti, Eritrea', 'Government of Djibouti', 'Government of Eritrea', 

1, 1, 2, '6/11/2008', '6/11/2008', 2008, 2008, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (428, 'Ethiopia', 'Government of Ethiopia', 'SALF', 1, 1, 4, '12/31/1977', 

'12/31/1980', 1977, 1980, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (429, 'Ethiopia', 'Government of Ethiopia', 'SLM', 1, 1, 3, '3/31/1983', 

'12/31/1983', 1983, 1983, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (430, 'India', 'Government of India', 'KNF', 1, 1, 3, '5/12/1997', 

'12/5/1997', 1997, 1997, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (431, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'MNDAA', 1, 1, 3, 

'8/29/2009', '8/29/2009', 2009, 2009, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (432, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'LNUP', 1, 1, 3, 

'2/28/1973', '12/31/1982', 1973, 1982, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (433, 'Mauritania', 'Government of Mauritania', 'AQIM', 2, 1, 3, '9/18/2010', 

'10/20/2011', 2010, 2011, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (434, 'Ethiopia', 'Government of Ethiopia', 'IGLF', 1, 1, 3, '10/10/1991', 

'10/10/1991', 1991, 1991, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (435, 'Sudan', 'Government of Sudan', 'Republic of South Sudan', 1, 1, 3, 

'5/19/2011', '6/15/2011', 2011, 2011, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (436, 'Libya', 'Government of Libya', 'Forces of Muammar Gaddafi NTC', 2, 2, 

3, '3/4/2011', '11/23/2011', 2011, 2011, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (437, 'India', 'Government of India', 'GNLA', 1, 1, 3, '11/15/2012', 

'12/21/2012', 2012, 2012, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (438, 'China', 'Government of China', 'ETIM', 1, 1, 3, '8/10/2008', 

'8/29/2008', 2008, 2008, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (439, 'Mali', 'Government of Mali', 'AQIM', 2, 1, 3, '6/16/2009', '7/16/2009', 

2009, 2009, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (440, 'Bangladesh', 'Government of Bangladesh', 'PBCP-Janajudhha', 2, 1, 3, 

'3/13/2005', '12/22/2006', 2005, 2006, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (441, 'South Sudan, Sudan', 'Government of South Sudan', 'Government of 

Sudan', 1, 1, 2, '3/27/2012', '12/26/2012', 2012, 2012, 4);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (442, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'NSCN-K', 1, 1, 3, 

'2/28/2000', '5/18/2001', 2000, 2001, 3);
INSERT INTO conflicts (conflictId, location, sideA, sideB, incompatibility, intensityLevel, typeOfConflict, startDate, 

endDate, startYear, endYear, region) VALUES (443, 'Myanmar (Burma)', 'Government of Myanmar (Burma)', 'NSCN-K', 1, 1, 3, 

'1/31/2005', '2/15/2007', 2005, 2007, 3);
