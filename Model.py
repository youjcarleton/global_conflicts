#!/usr/bin/python

"""Constructs HTML views based on cgi input"""

import cgi
from data_source import DataSource

def printPage(parameters):
    """Calls the appropriate function based on the contents of the 
    parameters dictionary."""

    if parameters['region']:
        printHTML(createCountriesView(parameters['region'], 'template.html'))
    elif parameters['country']:
        printHTML(createConflictsView(parameters['country'], 
            'template.html'))
    elif parameters['year']:
        printHTML(createYearView(parameters['year'], 'template.html'))
    elif parameters['page']:
        if parameters['page'] == 'region':
            printHTML(createRegionsView('template.html'))
        elif parameters['page'] == 'year':
            printHTML(createSelectYearView('template.html'))
        elif parameters['page'] == 'mostConflicts':
            printHTML(createCountriesByConflicts('template.html'))
        else:
            printHTML(createHomePage('template.html'))
    else:
        printHTML(createHomePage('template.html'))

def openTemplate(template_filename):
    """Opens a file and returns its contents"""
    try:
        f = open(template_filename)
        template_text = f.read()
        f.close()
    except Exception, e:
        template_text = "Cannot read template file <tt>%s</tt>." % (
            template_filename)
    return template_text

def createHomePage(template_filename):
    """Creates the home page"""

    template_text = openTemplate(template_filename)
    replacements = {}
    replacements["heading"] = """<h2>Find a post-WWII global conflict! Click 
        a link to get started</h2>"""
    replacements["navigation"] = createNavigation()
    replacements["body"] = """<p>Data is from Uppsala Conflict Data Program 
        (UCDP Conflict Encyclopedia: www.ucdp.uu.se/database, 
        Uppsala University)</p>"""

    outputText = template_text.format(**replacements)
    return outputText

def createSelectYearView(template_filename):
    """Creates a page allowing the user to select a year"""

    template_text = openTemplate(template_filename)
    replacements = {}
    replacements["heading"] = "<h2>Find post-WWII conflicts by year</h2>"
    replacements["navigation"] = createNavigation()
    replacements["body"] = "<form action='index.py' method='get'>"
    replacements["body"] += """<p>Select a year (e.g. 1992):</p>
        <p><input type="text" name="year"></p>"""
    replacements["body"] += "<p><input type='submit' value='Go'/></p></form>"
    outputText = template_text.format(**replacements)
    return outputText

def createRegionsView(template_filename):
    """Creates an HTML view displaying a list of regions"""

    template_text = openTemplate(template_filename)
    replacements = {}
    replacements["heading"] = "<h2>Find post-WWII conflicts by region</h2>"
    replacements["navigation"] = createNavigation()
    replacements["body"] = "<form action='index.py' method='get'>"
    replacements["body"] += """<p>Select a region:</p>
        <p><input type="radio" name="region" value="Africa"> Africa</p>
        <p><input type="radio" name="region" value="Asia"> Asia</p>
        <p><input type="radio" name="region" value="Europe"> Europe</p>
        <p><input type="radio" name="region" value="Americas"> Americas</p>
        <p><input type="radio" name="region" value="Middle East"> 
        Middle East</p>"""
    replacements["body"] += "<p><input type='submit' value='Go'/></p></form>"
    outputText = template_text.format(**replacements)
    return outputText

def createCountriesView(region, template_filename):
    """Creates an HTML view displaying a list of countries 
    in a given region"""

    template_text = openTemplate(template_filename)
    
    replacements = {}

    if region == "Europe": 
        region_index = 1
    elif region == "Middle East":
        region_index = 2
    elif region == "Asia":
        region_index = 3
    elif region == "Africa":
        region_index = 4
    else:
        region_index = 5

    source = DataSource()
    countries = source.getAllCountriesInRegion(region_index)
    replacements["heading"] = "<h2>Region: %s</h2>" % region
    replacements["navigation"] = createNavigation()
    replacements["body"] = "<form action='index.py' method='get'>"
    replacements["body"] += "<p>Select a country:</p>"
    
    for country in countries:
        replacements["body"] += (
            "<p><input type='radio' name='country' value='%s'> %s</p>" 
            % (country, country))
    replacements["body"] += "<p><input type='submit' value='Go'/></p></form>"
    
    outputText = template_text.format(**replacements)
    
    return outputText

def createConflictsView(country, template_filename):
    """Creates an HTML view displaying a table with information
    on conflicts that occurred in the given country"""

    template_text = openTemplate(template_filename)
    
    replacements = {}
    replacements["heading"] = "<h2>Conflicts for %s:<h2>" % (country)
    replacements["navigation"] = createNavigation()

    source = DataSource()
    conflictsId = source.getConflictByLocation(country)

    replacements["body"] = createConflictsTable(conflictsId)
    outputText = template_text.format(**replacements)
    return outputText

def createYearView(year, template_filename):
    template_text = openTemplate(template_filename)
    
    try:
        year = int(year)
        replacements = {}
        replacements["heading"] = "<h2>Conflicts for %s:" % (year)
        replacements["navigation"] = createNavigation()
        source = DataSource()
        conflictsId = source.getConflictByYear(year)
        replacements["body"] = createConflictsTable(conflictsId)

    except ValueError:
        replacements = {}
        replacements["heading"] = "<h2>Conflicts for %s:" % (year)
        replacements["navigation"] = createNavigation()
        replacements["body"] = """<h4>Invalid year inputted, try again: 
        the year should be in the format 1234.</h4>"""
    
    outputText = template_text.format(**replacements)
    return outputText

def createCountriesByConflicts(template_filename):
    template_text = openTemplate(template_filename)
    replacements = {}
    replacements["heading"] = "<h2>Conflicts per country</h2>"
    replacements["navigation"] = createNavigation()
    table_string = "<h4 style='font-weight:normal'>"
    table_string += """<table style='width:100%'><tr>
                <td><b>Country</b></td>
                <td><b>Number of conflicts</b></td></tr>"""
    source = DataSource()
    countries = source.getAllCountries()
    for country in countries:
        count = len(source.getConflictByLocation(country))
        table_string += "<td>%s</td>" % (country)
        table_string += "<td>%s</td></tr>" % (count)
    replacements["body"] = table_string
    outputText = template_text.format(**replacements)
    return outputText

def createConflictsTable(conflicts):
    """Creates a table with all relevant information for the given list 
    of conflict ids"""

    table_string = "<h4 style='font-weight:normal'>"
    table_string += """<table style='width:100%'><tr>
                <td><b>Location</b></td>
                <td><b>Start date</b></td><td><b>End Date</b></td>
                <td><b>Side A</b></td><td><b>Side B</b></td>
                <td><b>Intensity</b></td><td><b>Conflict Type</b></td></tr>"""
    source = DataSource()
    counter = 0
    for conflict in conflicts:
        table_string += "<tr>"
        location = source.getLocation(conflict)
        start_date = source.getStartDate(conflict)
        end_date = source.getEndDate(conflict)
        sideA = source.getSideA(conflict)
        sideB = source.getSideB(conflict)
        intensity = source.getIntensity(conflict)
        conflict_type = source.getConflictType(conflict)
        table_string += "<td>%s</td>" % (location)
        table_string += "<td>%s</td>" % (start_date)
        table_string += "<td>%s</td>" % (end_date)
        table_string += "<td>%s</td>" % (sideA)
        table_string += "<td>%s</td>" % (sideB)
        table_string += "<td>%s</td>" % (intensity)
        table_string += "<td>%s</td>" % (conflict_type)
        table_string += "</tr>"
    table_string += "</table>"
    table_string += """<p>*The intensity is a level between 1 and 5.</p>
    <p>*The conflict type is divided into four types: 
    extrasystemic, interstate, internal and internationalized internal.</p>"""
    if len(conflicts) == 0:
        table_string = "No data available, sorry"

    return table_string

def createNavigation():
    """Creates navigation links"""
    navigation_string = '''<h4 style='font-weight:normal'>
        <a href="index.py?page=home">Home</a>
        <a href="index.py?page=region">Search by region</a>
        <a href="index.py?page=year">Search by year</a>
        <a href="index.py?page=mostConflicts">Conflicts per country</a></h4>'''

    return navigation_string

def printHTML(output):
    print 'Content-type: text/html\r\n\r\n',
    print output