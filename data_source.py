#!/usr/bin/python
# -*- coding: utf-8 -*-

"""

Josh You and Lauren Yoo
Data Source Class
May 17, 2015

This python file is a data base interface that interacts with the database.
The database has two tables: conflicts, with the following 10 columns: 
conflict Id, location, sideA, sideB, incompatibility, 
intensityLevel, typeOfConflict, startDate, endDate, and region.
The next table is countries, with region as its sole column.
"""
import cgitb
cgitb.enable()
import os.path
import sys
import psycopg2



class DataSource():
    def __init__(self):
        USERNAME = 'youj'
        DB_NAME = 'youj'
        f = open(os.path.join('/cs257', USERNAME))
        PASSWORD = f.read().strip()
        f.close()
        db_connection = psycopg2.connect(user=USERNAME,
                                        database=DB_NAME,
                                        password=PASSWORD)
        cursor = db_connection.cursor()
        self.cursor = cursor
 
    def getConflictList(self):
        """Returns a list of all conflict ids"""
        try:
            self.cursor.execute("SELECT conflictId FROM conflicts")
            results = []
            for row in self.cursor:
                results.append(row[0])
            return self.sortByStartYear(results)
        except:
            print 'Problem with gettin the list of conflicts =(<br>'
     
    def getAllCountries(self):
        """Returns a list of all countries"""
        self.cursor.execute("SELECT name FROM countries")
        results = []
        for row in self.cursor:
            results.append(row[0])
        return sorted(results)

    def getAllCountriesInRegion(self, region):
        """Returns a list of all countries in a given region"""
        try:
            self.cursor.execute("""SELECT name FROM countries WHERE 
                region = (%s)""", (region,))
            results = []
            for row in self.cursor:
                if row[0] not in results:
                    results.append(row[0])
            return sorted(results)
        except:
            print 'Problem with getting the list of countries =('        
    def getConflictByLocation(self, location):
        """"Returns a list of conflict ids for conflicts that happened in the 
        given location"""
        try:
            self.cursor.execute("""SELECT conflictId FROM conflicts WHERE 
                location LIKE (%s)""", (location,))
            ids = []
            results = {}
            for row in self.cursor:
                ids.append(row[0])
            for id in ids:
                results.update({id:self.getStartYear(id)})
            return self.sortByStartYear(results)
        except:
            print 'Problem with getting conflict by location =(<br>'

    def getStartYear(self, conflict_id):
        self.cursor.execute("""SELECT startYear FROM conflicts WHERE 
            conflictId = (%s)""", (conflict_id,))
        for row in self.cursor:
            year = row[0]
        return year

    def getEndYear(self, conflict_id):
        self.cursor.execute("""SELECT endYear FROM conflicts WHERE 
            conflictId = (%s)""", (conflict_id,))
        for row in self.cursor:
            year = row[0]
        return year

    def getConflictByYear(self, year):
        """Returns a list of conflicts that happened in the given year"""
        try:
            ids = self.getConflictList()
            results = {}
            for id in ids:
                start = self.getStartYear(id) 
                end = self.getEndYear(id)
                if (start <= year) and (end >= year):
                    results.update({id: start})
            return self.sortByStartYear(results)

        except:
            print 'Problem with getting a list of conflicts by year =(<br>'
    
    @staticmethod
    def sortByStartYear(conflicts):
        """Given a dictionary of conflict ids and their start years, 
        returns a list of the ids sorted by start year in descending order"""
        return sorted(conflicts, key=conflicts.__getitem__, reverse = True)

    def getConflictByParticipant(self, participant):
        """Given a list of parties, returns a list of conflict ids 
        involving the given partipants"""

        try:
            self.cursor.execute("""SELECT conflictId FROM conflicts WHERE 
                sideA = (%s)""", (participant,))
            results = []
            for row in self.cursor:
                results.append(row)
            #append conflicts in which participant = side B     
            self.cursor.execute("""SELECT conflictId FROM conflicts WHERE 
                sideB = (%s)""", (participant,))
            for row in self.cursor:
                 results.append(row)
            return results
        except:
            print 'Problem with getting conflicts by participants'
             
    def getLocation(self, conflict_id):
        """Returns the location where the conflict occurred"""
        try:
            self.cursor.execute("""SELECT location FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            location = ""
            for row in self.cursor:
                location += str(row[0])
            return location
        except:
            print 'Problem with getLocation'
        
    def getStartDate(self, conflict_id):
        """Returns start date of the given conflict""" 
        try:
            self.cursor.execute("""SELECT startDate FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            startDate = ""
            for row in self.cursor:
                startDate += str(row[0])
            return startDate
        except:
            print 'Problem with getStartDate'
        
    def getEndDate(self, conflict_id):
        """Returns end date of the given conflict""" 
        try:
            self.cursor.execute("""SELECT endDate FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            endDate = ""
            for row in self.cursor:
                endDate += str(row[0])
            return endDate
        except:
            print 'Problem with getEndDate'
        
    def getSideA(self, conflict_id):
        """Returns a list of participants designated 
        Side A in a given conflict"""
        try:
            self.cursor.execute("""SELECT sideA FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            sideA = ""
            for row in self.cursor:
                sideA += str(row[0])
            return sideA
        except:
            print 'Problem with getsideA'
        
    def getSideB(self, conflict_id):
        """Returns a list of participants designated Side A in a given conflict"""
        try:
            self.cursor.execute("""SELECT sideB FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            sideB = ""
            for row in self.cursor:
                sideB += str(row[0])
            return sideB
        except:
            print 'Problem with getsideB'
        
    def getIntensity(self, conflict_id):
        """Returns intensity of the given conflict_id"""
        try:
            self.cursor.execute("""SELECT intensityLevel FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            intensityLevel = ""
            for row in self.cursor:
                intensityLevel += str(row[0])
            return intensityLevel
        except:
            print 'Problem with getIntensity'
        
    def getConflictType(self, conflict_id):
        """Returns the type of the given conflict_id 
        (Extra-systemic, interstate, internal, or international""" 
        try:
            self.cursor.execute("""SELECT typeOfConflict FROM conflicts WHERE 
                conflictId = (%s)""", (conflict_id, ))
            typeOfConflict = 0
            for row in self.cursor:
                typeOfConflict += row[0]
                if typeOfConflict == 1:
                    typeOfConflict = "Extra-systemic"
                elif typeOfConflict == 2:
                    typeOfConflict = "Inter-state"
                elif typeOfConflict == 3: 
                    typeOfConflict = "Internal"
                elif typeOfConflict == 4: 
                    typeOfConflict = "Internationalized internal"
                else:
                    typeOfConflict = " "
            return typeOfConflict
        except:
            print 'Problem with getConflictType'

    def getRegion(self, conflict_id):
        query = """SELECT region FROM conflicts WHERE 
        conflictId = (%s)""", (conflict_id,)
        try:
            self.cursor.execute(query)
            region = ""
            for row in self.cursor:
                region += str(row[0])
            return region
        except:
            print 'Problem with getting region'
