#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Global conflicts: a web application to find conflicts 
based on a country of interest.

By Josh You and Lauren Yoo, May 11, 2015.
"""

import cgi
import cgitb
cgitb.enable()
from data_source import DataSource
import Controller
import Model

def main():
    """Executes the web app, responding to the user's input and displaying the 
    appropriate view"""
    parameters = Controller.getCgiParameters()
    Model.printPage(parameters)

if __name__ == '__main__':
    main()