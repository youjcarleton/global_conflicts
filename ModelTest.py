import unittest
import Model

class ModelTest(unittest.TestCase):

    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        print "ModelTest:setUp_:begin"
        ## do something...
        print "ModelTest:setUp_:end"
     
    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        print "ModelTest:tearDown_:begin"
        ## do something...
        print "ModelTest:tearDown_:end"

    # tests that CreateYearView gives the correct error message
    def testCreateYearView(self):
        assert ("Invalid year inputted, try again" 
            in Model.createYearView("kittens", "template.html"))

if __name__ == "__main__":
   unittest.main()