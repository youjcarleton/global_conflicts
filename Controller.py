#!/usr/bin/env python

import cgi

def getCgiParameters():
    """Returns a dictionary of sanitized values for the CGI parameters
    """
    form = cgi.FieldStorage()
    parameters = {'page': '', 'region':'', 'country':'', 
        'conflict':'', 'year':''}
    
    if 'page' in form:
        parameters['page'] = sanitizeUserInput(form['page'].value)

    if 'region' in form:
        parameters['region'] = sanitizeUserInput(form['region'].value)

    if 'country' in form:
        parameters['country'] = sanitizeUserInput(form['country'].value)
        
    if 'conflict' in form:
        parameters['conflict'] = sanitizeUserInput(form['conflict'].value)

    if 'year' in form:
        parameters['year'] = sanitizeUserInput(form['year'].value)

    return parameters

def sanitizeUserInput(s):
    """Strips out scary characters from s and returns the sanitized version.
    """
   
    chars_to_remove = ";,\\/:'\"<>@"
    for ch in chars_to_remove:
        s = s.replace(ch, '')
    return s