import unittest
import Controller

class ControllerTest(unittest.TestCase):

    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        print "ControllerTest:setUp_:begin"
        ## do something...
        print "ControllerTest:setUp_:end"
     
    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        print "ControllerTest:tearDown_:begin"
        ## do something...
        print "ControllerTest:tearDown_:end"

    # tests if SanitizeUserInput removes unwanted characters
    def testSanitizeUserInput(self):
        scary_string = ";,\\/:'\"<>@"
        assert Controller.sanitizeUserInput(scary_string) == ""
        # should preserve other characters
        for ch in scary_string:
            assert Controller.sanitizeUserInput("Kenya%s" % (ch)) == "Kenya"
        print "ControllerTest:testSanitizeUserInput"

if __name__ == "__main__":
   unittest.main()